<?php

use App\Http\Controllers\Calculations\CalculateAllEmissionsController;
use App\Http\Controllers\Calculations\CalculateVehicleEmissionsController;
use App\Http\Controllers\Calculations\VehicleCommutesAndJourneysController;
use App\Http\Controllers\Commutes\CommutesController;
use App\Http\Controllers\Countries\CountryController;
use App\Http\Controllers\Diet\UserDietController;
use App\Http\Controllers\Flights\FlightController;
use App\Http\Controllers\Flights\FlightEmissionsController;
use App\Http\Controllers\Fuels\FuelsController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Journeys\JourneyController;
use App\Http\Controllers\Motorbikes\MotorbikeController;
use App\Http\Controllers\Users\EmailVerificationController;
use App\Http\Controllers\Users\ProfileController;
use App\Http\Controllers\Users\UserController;
use App\Http\Controllers\Users\UserGenderController;
use App\Http\Controllers\Users\UserMeasurementController;
use App\Http\Controllers\Users\UserVehicleEcoProfileController;
use App\Http\Controllers\Users\UserVehicleUsageController;
use App\Http\Controllers\Vehicles\VehiclesController;
use App\Http\Controllers\Newsfeed\NewsfeedController;
use App\Http\Controllers\Users\UserNewsFeedController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('v1/auth')->group(function () {
    Route::post('/signup', [UserController::class, 'signup']);
    Route::post('/verify', [EmailVerificationController::class, 'verify'])->name('verification');
    Route::post('/signin', [UserController::class, 'signin']);
    Route::post('/resend/code', [EmailVerificationController::class, 'resendVerification']);
    Route::get('/list/gender', [UserGenderController::class, 'getGenderList']);
    Route::get('/list/measurement', [UserMeasurementController::class, 'getPreferredMeasurementList']);
});

Route::middleware('auth:api')->prefix('v1/admin')->group(function () {
    Route::get('/list/users', [UserController::class, 'listUsers']);
    Route::get('/user/{id}', [UserController::class, 'getSingleUser']);
});

Route::middleware('auth:api')->prefix('v1/user')->group(function () {
    Route::get('/profile/{id}', [ProfileController::class, 'profile']);
    Route::patch('/profile/update/{id}', [ProfileController::class, 'updateProfile']);
    Route::post('/profile/image/update/{id}', [ProfileController::class, 'uploadProfilePic']);
    Route::put('/owns/vehicle/{id}', [ProfileController::class, 'setIfUserOwnsAVehicle']);
});

Route::prefix('v1/countries')->group(function () {
    Route::get('all', [CountryController::class, 'countries']);
    Route::get('codes', [CountryController::class, 'getCountryCodes']);
});

Route::middleware('auth:api')->prefix('v1/newsfeed')->group(function () {
    // Route::get('all', [NewsfeedController::class, 'newsfeed']);
    Route::get('news', [NewsfeedController::class, 'getNewsfeed']);
    // Route::post('create/newsfeed', [NewsfeedController::class, 'createNewsfeed']);
    Route::get('news/{id}', [NewsfeedController::class, 'getUserNews']);
    Route::get('user/{id}', [NewsfeedController::class, 'getUserNewsFeed']);
});
Route::prefix('v1/newsfeed')->group(function () {
    Route::get('all', [NewsfeedController::class, 'newsfeed']);
    Route::post('create/newsfeed', [NewsfeedController::class, 'createNewsfeed']);
});


Route::middleware('auth:api')->prefix('v1/profile/newsfeed')->group(function () {
    Route::post('create', [UserNewsFeedController::class, 'createUserNewsFeed']);
});


Route::middleware('auth:api')->prefix('v1/vehicles')->group(function () {
    Route::get('all', [VehiclesController::class, 'getVehicles'])->withoutMiddleware('auth:api');
    Route::get('user/{id}', [VehiclesController::class, 'getUserVehicles']);
    Route::post('user/commute/create', [UserVehicleUsageController::class, 'createVehicleCommuteUsage']);
    Route::post('/footprint/user/{id}', [CalculateVehicleEmissionsController::class, 'calculateEmissionBasedOnUsage']);
    Route::get('footprint/all/user/{id}', [CalculateVehicleEmissionsController::class, 'vehicleEmissionsBreakDown']);
    Route::post('user/journey/create', [UserVehicleUsageController::class, 'createVehicleJourneyUsage']);
    Route::get('user/commute/{commuteId}', [UserVehicleUsageController::class, 'getSingleCommute']);
    Route::get('user/journey/{journeyId}', [UserVehicleUsageController::class, 'getSingleJourney']);
});

Route::prefix('v1/fuels')->group(function () {
    Route::get('all', [FuelsController::class, 'getFuels']);
});

Route::middleware('auth:api')->prefix('v1/eco/vehicles')->group(function () {
    Route::post('create', [UserVehicleEcoProfileController::class, 'createVehicleEcoProfile']);
    Route::get('/{id}', [UserVehicleEcoProfileController::class, 'getVehicleEcoProfile']);
    Route::patch('/update/{id}', [UserVehicleEcoProfileController::class, 'updateSingleVehicle']);
});

Route::middleware('auth:api')->prefix('v1/journeys')->group(function () {
    Route::get('all', [JourneyController::class, 'getAllJourneys'])->withoutMiddleware('auth:api');
    Route::get('user/{id}', [JourneyController::class, 'getUserJourneys']);
    Route::patch('user/update/{id}', [JourneyController::class, 'updateUserJourney']);
});

Route::middleware('auth:api')->prefix('v1/commutes')->group(function () {
    Route::get('user/{id}', [CommutesController::class, 'filterUserCommutes']);
    Route::patch('user/update/{id}', [CommutesController::class, 'updateUserCommute']);
});

Route::middleware('auth:api')->prefix('v1/flights')->group(function () {
    Route::get('all', [FlightController::class, 'getFlightTypes'])->withoutMiddleware('auth:api');
    Route::get('classes/all', [FlightController::class, 'getFlightClasses'])->withoutMiddleware('auth:api');
    Route::get('commercial', [FlightController::class, 'getCommercialPlanes'])->withoutMiddleware('auth:api');
    Route::get('private', [FlightController::class, 'getPrivatePlanes'])->withoutMiddleware('auth:api');
    Route::post('create/usage', [FlightController::class, 'createFlightUsage']);
    Route::get('user/all/{id}', [FlightController::class, 'getUserFlights']);
    Route::post('calculate/emission/{id}', [FlightEmissionsController::class, 'calclulateFlightEmission']);
    Route::put('user/update/{userId}/{flightId}', [FlightController::class, 'updateFlight']);
    Route::get('user/{flightId}', [FlightController::class, 'getUserFlight']);
});

Route::middleware('auth:api')->prefix('v1/commutes/journeys')->group(function () {
    Route::get('user/comparison/{id}', [VehicleCommutesAndJourneysController::class, 'compareCommutesToJourneys']);
});

Route::prefix('v1/motorbikes')->group(function () {
    Route::get('all', [MotorbikeController::class, 'getMotorbikes']);
});

Route::middleware('auth:api')->prefix('v1/houses')->group(function () {
    Route::get('details', [HomeController::class, 'getHouseDetails'])->withoutMiddleware('auth:api');
    Route::post('details/create', [HomeController::class, 'storeUserHomeDetails']);
    Route::get('details/retrieve/{id}', [HomeController::class, 'getUserHouseDetails']);
    Route::post('calculate/emission/{id}', [HomeController::class, 'calculateHouseHoldFactors']);
    Route::get('user/{usageId}', [HomeController::class, 'getHouseHoldUsage']);
    Route::put('user/update/{usageId}', [HomeController::class, 'updateHouseHoldUsage']);
});

Route::middleware('auth:api')->prefix('v1/diet')->group(function () {
    Route::get('details', [UserDietController::class, 'dietDetails']);
    Route::get('details/retrieve/{id}', [UserDietController::class, 'getUserDietDetails']);
    Route::post('details/create', [UserDietController::class, 'storeDietDetails']);
    Route::post('calculate/emission/{id}', [UserDietController::class, 'userDietEmission']);
    Route::get('user/{usageId}', [UserDietController::class, 'getSingleDietUsage']);
    Route::put('user/update/{usageId}', [UserDietController::class, 'updateDietUsage']);
});

Route::middleware('auth:api')->prefix('v1/total')->group(function () {
    Route::get('footprint/user/{id}', [CalculateAllEmissionsController::class, 'calculateAllCombinedEmissions']);
});

Route::middleware('auth:api')->prefix('v1/payment')->group(function () {
    Route::post('make/user/{id}', [CalculateAllEmissionsController::class, 'makePaymentCombinedEmissions']);

    Route::get('make/user/response', [CalculateAllEmissionsController::class, 'paymentResponse']);
});


Route::post('/stk/push/callback/url', [CalculateAllEmissionsController::class, 'mpesaRes']);
