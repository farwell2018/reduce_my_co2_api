## Reduce My CO2

## What is this repository for?
- Reduce My CO2 is a mobile app for tracking and calculating an individuals CO2 footprint.

## How do I get a copy of this project setup?
- Clone this repository `$ git clone https://j_musila@bitbucket.org/farwell2018/reduce_my_co2_api.git`

## Contribution Guidelines
- Run `$ composer install` to install all the project dependancies
- Copy the .env_example file and rename it to .env
- Create the database and and all the required environment variables
- Run `$ php artisan key:generate` to generate app key
- Run `$ php artisan jwt:secret` to generate jwt secret key
- Run `$ php artisan migrate` to get the DB tables created
- Run `$ php artisan serve` to the get started and have fun!
- Incase of any bug or additional feature,
        - Create a feature/bug branch from the develop branch
        - Raise a PR and wait for review 🚀
- Run `$ php artisan test` or `./vendor/bin/pest` for running the tests

## PostMan Collection
- [Link to PostMan Collection!](https://www.getpostman.com/collections/45ba7e3384a3c221520c)
- Set two environments on Postman, one for Production other for