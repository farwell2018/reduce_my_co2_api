<?php

use App\Models\Fuel;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class, RefreshDatabase::class);

$default_url = '/api/v1/fuels';

it('can get a list of all fuels', function() use($default_url){
    $fuels = Fuel::factory()->raw();
    $response = $this->getJson("$default_url/all", $fuels);
    $response->assertStatus(200)->assertJson([
       'msg' => 'Fuels retrieved successfully.'
    ]);
});