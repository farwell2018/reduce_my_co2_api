<?php

use App\Models\User;
use App\Models\UserVehicleEcoProfile;
use App\Models\UserVehicleUsage;
use App\Models\Vehicle;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class, RefreshDatabase::class);

$default_url = '/api/v1/vehicles';

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->vehicle = Vehicle::factory()->create();
    $this->eco_profile = UserVehicleEcoProfile::factory()->create();
});

it('can get a list of all vehicles', function () use ($default_url) {
    $vehicles = $this->vehicle->toArray();
    $response = $this->getJson("$default_url/all", $vehicles);
    $response->assertStatus(200)->assertJson([
       'msg' => 'Vehicles retrieved successfully.'
    ]);
});

test('user can get all their vehicles', function () use ($default_url) {
    $vehicles = $this->vehicle->toArray();
    $id = $this->user->id;
    $response = $this->be($this->user)->getJson("$default_url/user/$id", $vehicles);
    $response->assertStatus(200);
});

test('user can create commutes', function () use ($default_url) {
    $vehicle = [
        'user_id' => $this->user->id,
        'user_vehicle_eco_profile_id' => $this->eco_profile->id,
        'vehicle_name' => $this->eco_profile->vehicle_name,
        'usage_type' => UserVehicleUsage::COMMUTE_USAGE,
        'days_per_week' => 4,
        'distance_per_day' => 50 * 2,
        'distance_per_week' => 50 * 2 * 4
    ];
    $response = $this->be($this->user)->postJson("$default_url/user/commute/create", $vehicle);
    $response->assertStatus(201)->assertJson([
        'msg' => 'User vehicle usage created successfully'
    ]);
});

test('user cannot create commutes with invalid vehicle name', function () use ($default_url) {
    $vehicle = [
        'user_id' => $this->user->id,
        'user_vehicle_eco_profile_id' => $this->eco_profile->id,
        'vehicle_name' => 'Subaru',
        'usage_type' => UserVehicleUsage::COMMUTE_USAGE,
        'days_per_week' => 4,
        'distance_per_day' => 50 * 2,
        'distance_per_week' => 50 * 2 * 4
    ];
    $response = $this->be($this->user)->postJson("$default_url/user/commute/create", $vehicle);
    $response->assertStatus(404)->assertJson([
        'msg' => 'Missing or Invalid Vehicle name. Try again'
    ]);
});

test('user cannot create commutes without vehicle name', function () use ($default_url) {
    $vehicle = [
        'user_id' => $this->user->id,
        'user_vehicle_eco_profile_id' => $this->eco_profile->id,
        'vehicle_name' => '',
        'usage_type' => UserVehicleUsage::COMMUTE_USAGE,
        'days_per_week' => 4,
        'distance_per_day' => 50 * 2,
        'distance_per_week' => 50 * 2 * 4
    ];
    $response = $this->be($this->user)->postJson("$default_url/user/commute/create", $vehicle);
    $response->assertStatus(422)->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
            "vehicle_name" => [
                "The vehicle name field is required."
            ]
        ]
    ]);
});

test('user can get a single commute', function () use ($default_url) {
    $vehicle = [
        'user_id' => $this->user->id,
        'user_vehicle_eco_profile_id' => $this->eco_profile->id,
        'vehicle_name' => $this->eco_profile->vehicle_name,
        'usage_type' => UserVehicleUsage::COMMUTE_USAGE,
        'days_per_week' => 4,
        'distance_per_day' => 50 * 2,
        'distance_per_week' => 50 * 2 * 4
    ];
    $response = $this->be($this->user)->postJson("$default_url/user/commute/create", $vehicle);
    $response->assertStatus(201)->assertJson([
        'msg' => 'User vehicle usage created successfully'
    ]);
    $response1 = $this->be($this->user)->getJson("$default_url/user/commute/1");
    $response1->assertStatus(200)->assertJson([
        'msg' => 'Commute retrieved successfully'
    ]);
});

test('user can get a single journey', function () use ($default_url) {
    $this->withoutExceptionHandling();
    $vehicle = [
        'user_id' => $this->user->id,
        'user_vehicle_eco_profile_id' => $this->eco_profile->id,
        'vehicle_name' => $this->eco_profile->vehicle_name,
        'usage_type' => UserVehicleUsage::JOURNEY_USAGE,
        'days_per_week' => 4,
        'distance_per_day' => 50 * 2,
        'distance_per_week' => 50 * 2 * 4
    ];
    $response = $this->be($this->user)->postJson("$default_url/user/commute/create", $vehicle);
    $response->assertStatus(201)->assertJson([
        'msg' => 'User vehicle usage created successfully'
    ]);
    $response1 = $this->be($this->user)->getJson("$default_url/user/journey/1");
    $response1->assertStatus(200)->assertJson([
        'msg' => 'Journey retrieved successfully'
    ]);
});

test('user can create journeys', function () use ($default_url) {
    $vehicle = [
        'user_id' => $this->user->id,
        'user_vehicle_eco_profile_id' => $this->eco_profile->id,
        'vehicle_name' => $this->eco_profile->vehicle_name,
        'usage_type_id' => UserVehicleUsage::JOURNEY_USAGE,
        'days_per_week' => 4,
        'distance_per_day' => 50 * 2,
        'distance_per_week' => 50 * 2 * 4
    ];
    $response = $this->be($this->user)->postJson("$default_url/user/journey/create", $vehicle);
    $response->assertStatus(201)->assertJson([
        'msg' => 'User vehicle usage created successfully'
    ]);
});
