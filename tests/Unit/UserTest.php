<?php

use App\Mail\EmailVerificationMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;

uses(Tests\TestCase::class, RefreshDatabase::class);

$default_url = '/api/v1/auth';
$profile_url = '/api/v1/user';
$admin_url = '/api/v1/admin';

$missing_first_name = [
    'last_name' => 'Doe',
    'password' => '123456',
    'email' => 'johndoe@gmail.com',
    'phone_number' => '792707534'
];

$missing_last_name = [
    'first_name' => 'John',
    'password' => '123456',
    'email' => 'johndoe@gmail.com',
    'phone_number' => '792707534'
];

$missing_phone_number = [
    'first_name' => 'John',
    'last_name' => 'Doe',
    'password' => '123456',
    'email' => 'johndoe@gmail.com'
];

$missing_password = [
    'first_name' => 'John',
    'last_name' => 'Doe',
    'email' => 'johndoe@gmail.com',
    'phone_number' => '792707534'
];

beforeEach(function () {
    $this->test_user = User::factory()->create();
});

it('can successfully create a user', function () use ($default_url) {
    Mail::fake();
    $test_user = User::factory()->raw();
    $test_user_email = User::factory()->create();
    $response = $this->postJson("{$default_url}/signup", $test_user);
    $response->assertStatus(201)->assertJson([
        'msg' => 'User created successfully'
    ]);
    Mail::to($test_user_email->email)->send(new EmailVerificationMail(['user' => 'Test email']));
    Mail::assertSent(EmailVerificationMail::class);
    $this->assertDatabaseHas('users', ['id' => $test_user_email->id]);
});

it('cannot create user without first name', function () use ($default_url, $missing_first_name) {
    $response = $this->postJson("{$default_url}/signup", $missing_first_name);
    $response->assertStatus(422)->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
            "first_name" => [
                "The first name field is required."
            ]
        ]
    ]);
});

it('cannot create user without last name', function () use ($default_url, $missing_last_name) {
    $response = $this->postJson("{$default_url}/signup", $missing_last_name);
    $response->assertStatus(422)->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
            "last_name" => [
                "The last name field is required."
            ]
        ]
    ]);
});

it('cannot create user without phone number', function () use ($default_url, $missing_phone_number) {
    $response = $this->postJson("{$default_url}/signup", $missing_phone_number);
    $response->assertStatus(422)->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
            "phone_number" => [
                "The phone number field is required."
            ]
        ]
    ]);
});

it('cannot create user without password', function () use ($default_url, $missing_password) {
    $response = $this->postJson("{$default_url}/signup", $missing_password);
    $response->assertStatus(422)->assertJson([
        "message" => "The given data was invalid.",
        "errors" => [
            "password" => [
                "The password field is required."
            ]
        ]
    ]);
});

test('user can verify email', function () use ($default_url) {
    $user = $this->test_user;
    $verified_user = [
        'verified_at' => Carbon::now(),
        'verification_code' => $user->verification_code
    ];
    $response = $this->postJson("{$default_url}/verify", $verified_user);
    $response->assertStatus(200)->assertJson([
        'msg' => 'Email verified successfully, proceed to login'
    ]);
    $this->assertDatabaseHas('users', ['email' => $user->email]);
    $response1 = $this->postJson("{$default_url}/verify", $verified_user);
    $response1->assertStatus(409)->assertJson([
        'msg' => 'This email has already been verified.'
    ]);
});

it('cannot verify unexisting user', function () use ($default_url) {
    $verified_user = ['verified_at' => Carbon::now()];
    $response = $this->postJson("{$default_url}/verify", $verified_user);
    $response->assertStatus(404)->assertJson([
        'msg' => 'User with that verification code does not exist'
    ]);
});

it('can resend verification code', function () use ($default_url) {
    $user = $this->test_user;
    $email = [
        'email' => $user->email
    ];
    $response = $this->postJson("{$default_url}/resend/code", $email);
    $response->assertStatus(200)->assertJson([
        'msg' => 'Verification code resend, please check your email'
    ]);
});

it('cannot resend verification code to unexisting user', function () use ($default_url) {
    $this->test_user;
    $email = [
        'email' => 'test@gmail.com'
    ];
    $response = $this->postJson("{$default_url}/resend/code", $email);
    $response->assertStatus(404)->assertJson([
        'msg' => 'User with that email address not found'
    ]);
});

test('user can login successfully', function () use ($default_url) {
    $user = $this->test_user;
    $verified_user = [
        'verified_at' => Carbon::now(),
        'verification_code' => $user->verification_code
    ];
    $response = $this->postJson("{$default_url}/verify", $verified_user);
    $response->assertStatus(200)->assertJson([
        'msg' => 'Email verified successfully, proceed to login'
    ]);
    $credentials = [
        'password' => '123456',
        'email' => $user->email
    ];
    $response = $this->postJson("{$default_url}/signin", $credentials);
    $response->assertStatus(200)->assertJson([
        'msg' => 'User logged in successfully.'
    ]);
});

test('user cannot login with invalid credentials', function () use ($default_url) {
    $user = $this->test_user;
    $verified_user = [
        'verified_at' => Carbon::now(),
        'verification_code' => $user->verification_code
    ];
    $response = $this->postJson("{$default_url}/verify", $verified_user);
    $response->assertStatus(200)->assertJson([
        'msg' => 'Email verified successfully, proceed to login'
    ]);
    $credentials = [
        'email' => $user->email,
        'password' => $user->password
    ];
    $response = $this->postJson("{$default_url}/signin", $credentials);
    $response->assertStatus(401)->assertJson([
        'msg' => 'Invalid email or password. Try again.'
    ]);
});

test('user cannot login without email address', function () use ($default_url) {
    $user = $this->test_user;
    $credentials = [
        'password' => $user->password
    ];
    $response = $this->postJson("{$default_url}/signin", $credentials);
    $response->assertStatus(422);
});

test('user cannot login without password', function () use ($default_url) {
    $user = $this->test_user;
    $credentials = [
        'email' => $user->email
    ];
    $response = $this->postJson("{$default_url}/signin", $credentials);
    $response->assertStatus(422);
});

test('user cannot login before verifying their account', function() use($default_url){
    $user = $this->test_user;
    $credentials = [
        'email' => $user->email,
        'password' => $user->password
    ];
    $response = $this->postJson("{$default_url}/signin", $credentials);
    $response->assertStatus(422)->assertJson([
        'msg' => 'Please verify your account then proceed to login'
    ]);
});

test('admin can get a list of all users', function() use($admin_url){
    $user = User::factory()->create([
        'role_id' => User::IS_ADMIN
    ]);
    $response = $this->be($user)->getJson("{$admin_url}/list/users");
    $response->assertStatus(200)->assertJson([
        'msg' => 'Users retrieved successfully'
    ]);
});

test('normal user cannot retrieve a list of users', function() use($admin_url){
    $response = $this->be($this->test_user)->getJson("{$admin_url}/list/users");
    $response->assertStatus(401)->assertJson([
        'msg' => 'You have no access to this route, please contact your admin for help'
    ]);
});


test('a logged in user can get their profile', function() use($profile_url){
    $response = $this->be($this->test_user)->getJson("{$profile_url}/profile/{$this->test_user->id}");
    $response->assertStatus(200)->assertJson([
        'msg' => 'User profile retrieved successfully'
    ]);
});

test('a logged in user can update their profile', function() use($profile_url){
    $data = [
        'gender' => 'Female',
        'year_of_birth' => '1990',
        'twitter_handle' => 'johndoe',
        'preferred_measurement' => 'Metric',
        'country' => "Kenya"
    ];
    $response = $this->be($this->test_user)->patchJson("{$profile_url}/profile/update/{$this->test_user->id}", $data);
    $response->assertStatus(200)->assertJson([
        'msg' => 'Profile updated successfully'
    ]);
});