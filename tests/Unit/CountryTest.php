<?php

use App\Models\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class, RefreshDatabase::class);

$default_url = '/api/v1/countries';

it('can get a list of all countries', function() use($default_url){
    $countries = Country::factory()->raw();
    $response = $this->getJson("$default_url/all", $countries);
    $response->assertStatus(200)->assertJson([
       'msg' => 'Countries retrived successfully.'
    ]);
});
