<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function handleExceptions($request, Exception $exception)
    {
        if ($exception instanceof NotFoundHttpException) {
            $message = $exception->getMessage() ? $exception->getMessage() : "Endpoint not found. Please try again.";
            return response()->json([
                'msg' => $message
            ], 404);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            $message = $exception->getMessage() ? $exception->getMessage() : "Method not allowed. Please try again.";
            return response()->json([
                'msg' => $message
            ], 405);
        }
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Exception $e, $request) {
            return $this->handleExceptions($request, $e);
        });
    }
}
