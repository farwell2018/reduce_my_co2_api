<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class RegisterAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating an admin user';

    /**
     * User model.
     *
     * @var object
     */
    private $user;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $details = $this->getDetails();

        $admin = $this->user->createAdminUser($details);

        $this->display($admin);
    }

    /**
     * Get admin details
     *
     * @return array
     */
    public function getDetails()
    {
        $details['first_name'] = $this->ask('First Name');
        $details['last_name'] = $this->ask('Last Name');
        $details['phone_number'] = $this->ask('Phone Number');
        $details['email'] = $this->ask('Email');
        $details['password'] = Hash::make($this->secret('Password'));

        if (strlen($details['password']) < 6) {
            $this->error('Password must be more that six characters');
        }

        return $details;
    }

    /**
     * Display created admin.
     *
     * @param array $admin
     * @return void
     */
    private function display(User $admin): void
    {
        $headers = ['First Name', 'Last Name', 'Phone Number', 'Email'];

        $fields = [
            'First Name' => $admin->first_name,
            'Last Name' => $admin->last_name,
            'Phone Number' => $admin->phone_number,
            'Email' => $admin->email
        ];

        $this->info('Admin user created successfully');
        $this->table($headers, [$fields]);
    }
}
