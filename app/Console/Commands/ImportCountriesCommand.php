<?php

namespace App\Console\Commands;

use App\Models\Country;
use Illuminate\Console\Command;

class ImportCountriesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:countries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports all countries and creates them in the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $json = file_get_contents('./data/country_list.json');

        $countriesList = json_decode($json);

        Country::truncate();

        return collect($countriesList)->map(function($country){
            return $this->createCountry($country);
        });
    }

    public function createCountry($data)
    {
        $country= new Country([
            'name' => $data->name,
            'code' => $data->code,
            'status' => 0,
            'tel_code' => $data->dial_code,
            'flag' => $data->flag
        ]);

        $country->save();

        print ".";
    }
}
