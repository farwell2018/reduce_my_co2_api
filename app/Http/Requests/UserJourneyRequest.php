<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserJourneyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_name' => 'required|string',
            'days_per_week' => 'required|integer',
            'one_way_distance' => 'required|integer',
            'journey' => 'required|string'
        ];
    }
}
