<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleEcoProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vehicle_name' => 'required|string',
            // 'vehicle_type' => 'string',
            'vehicle_fuel_type' => 'required|string',
            // 'motorbike_type' => 'string'
            'fuel_per_week' => 'required|integer',
        ];
    }
}
