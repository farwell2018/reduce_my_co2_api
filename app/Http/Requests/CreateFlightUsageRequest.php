<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateFlightUsageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'flight_type' => 'required|string',
            'flight_class' => 'required|string',
            'number_of_passengers' => 'required|integer',
            'one_way_or_round' => 'required|integer',
            'duration' => 'required',
            'model' => 'required|string',
            'engine_type' => 'required|string'
        ];
    }
}
