<?php

namespace App\Http\Controllers\Fuels;

use App\Http\Controllers\Controller;
use App\Models\Fuel;
use Illuminate\Http\Request;

class FuelsController extends Controller
{
    /**
     * Get all fuels for Eco Profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFuels()
    {
        $fuels = Fuel::all();

        return response()->json([
            'data' => $fuels,
            'status' => 'success',
            'msg' => 'Fuels retrieved successfully.'
        ], 200);
    }
}
