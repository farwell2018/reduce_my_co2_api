<?php

namespace App\Http\Controllers\Vehicles;

use App\Http\Controllers\Controller;
use App\Models\UserVehicleEcoProfile;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Auth;

class VehiclesController extends Controller
{
    /**
     * Get all vehicles for Eco Profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVehicles()
    {
        $vehicles = VehiclesController::listVehicles();

        return response()->json([
            'data' => $vehicles,
            'status' => 'success',
            'msg' => 'Vehicles retrieved successfully.'
        ], 200);
    }

    /**
     * Statically get a list of Vehicles
     *
     * @return \Illuminate\Http\Response
     */
    public static function listVehicles()
    {
        return Vehicle::all();
    }

    /**
     * Get all vehicles belonging to a single user
     *
     * @param user_id int
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserVehicles($id)
    {
        $user_id = Auth::user()->id;

        if ($user_id == $id) {
            $vehicles = UserVehicleEcoProfile::where('user_id', $id)->get();

            return response()->json([
                'data' => $vehicles,
                'status' => 'success',
                'msg' => 'User vehicles retrieved successfully'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this account. Kindly login and view your account.'
            ], 401);
        }
    }
}
