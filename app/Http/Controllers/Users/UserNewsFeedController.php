<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Newsfeed;
use App\Models\UserNewsFeed;
use Illuminate\Support\Facades\Auth;

class UserNewsFeedController extends Controller
{
    /**
     * Create Eco Profile from Vehicle
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createUserNewsFeed(Request $request)
    {
        $newsfeed = $request->input('newsfeed');

        $newsfeed = Newsfeed::where('id', $newsfeed)->first();

        // if (!empty($newsfeed)) {
        //     $newsfeed = Newsfeed::where('id', $newsfeed)->first();

        //     if (empty($newsfeed)) {
        //         return response()->json([
        //             'status' => 'error',
        //             'msg' => 'Invalid NewsFeed. Please Try again'
        //         ], 404);
        //     }
        // }

   

        $newsfeedProfile = UserNewsFeed::firstOrCreate([
            'user_id' => Auth::user()->id,
            'newsfeed' => $newsfeed->id,

        ]);

        $newsfeedProfile->save();
    //         if ($newsfeedProfile->save()) {
    //     $newsfeedProfile->update([
    //                   'user_id' => Auth::user()->id,
    //         'newsfeed' => $newsfeed->title,
    //         ]);
    // }

        return response()->json([
            'data' => $newsfeedProfile,
            'status' => 'success',
            'msg' => 'News Feed Profile Created successfully'
        ], 201);
    }
}
