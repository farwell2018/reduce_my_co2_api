<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserMeasurementController extends Controller
{
    /**
     * Get a list of all measurement types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPreferredMeasurementList()
    {
        $list = User::getMeasurementMethod();

        return response()->json([
            'data' => $list,
            'status' => 'success',
            'msg' => 'Measurement list retrieved successfully.'
        ], 200);
    }
}
