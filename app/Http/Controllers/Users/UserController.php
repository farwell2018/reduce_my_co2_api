<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Countries\CountryController;
use App\Http\Requests\UserRegistrationRequest;
use App\Http\Requests\UserSigninRequest;
use App\Jobs\EmailVerificationJob;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    /**
     * Sign Up User
     *
     * @return Json
     */
    public function signup(UserRegistrationRequest $request)
    {
        $user = new User([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'other_name' => $request->input('other_name'),
            'email' => $request->input('email'),
            'phone_number' => $request->input('code') . $request->input('phone_number'),
            'username' => $request->input('username'),
            'gender' => $request->input('gender'),
            'country_id' => $request->input('country_id'),
            'role_id' => User::IS_REGULAR_USER,
            'year_of_birth' => $request->input('year_of_birth'),
            'twitter_handle' => $request->input('twitter_handle'),
            'password' => Hash::make($request->input('password')),
            'verification_code' => User::generateVerificationCode()
        ]);

        $data = [
            'name' => $request->get('first_name'),
            'email' => $request->get('email'),
            'code' => $user->verification_code
        ];

        EmailVerificationJob::dispatch($data);

        $login_data = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        $user->save();

        if (!$token = JWTAuth::attempt($login_data)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Invalid email or password. Try again.'
            ], 401);
        }

        return response()->json([
            'data' => $user,
            'access_token' => $token,
            'status' => 'success',
            'msg' => 'User created successfully'
        ], 201);
    }

    /**
     * Login user
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function signin(UserSigninRequest $request)
    {
        $user = User::where('email', $request->validated()['email'])->first();

        if (is_null($user)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Invalid email or password. Try again.'
            ], 401);
        }

        if ($user->isVerified()) {
            if (!$token = JWTAuth::attempt($request->validated())) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Invalid email or password. Try again.'
                ], 401);
            }

            return response()->json([
                'access_token' => $token,
                'user_id' => $user->id,
                'status' => 'success',
                'msg' => 'User logged in successfully.'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Please verify your account then proceed to login'
            ], 422);
        }
    }

    /**
     * Admin get a list of all users
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listUsers()
    {
        $loggedInUser = Auth::user();

        $users = User::with('country')->get();

        if ($loggedInUser->role_id != User::IS_ADMIN) {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this route, please contact your admin for help'
            ], 401);
        } else {
            return response()->json([
                'data' => $users,
                'status' => 'success',
                'msg' => 'Users retrieved successfully'
            ], 200);
        }
    }

    /**
     * Admin view a single user
     *
     * @param userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSingleUser($userId)
    {
        $loggedInUser = Auth::user();

        $user = User::where('id', $userId)->first();

        if ($loggedInUser->role_id != User::IS_ADMIN) {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this route, please contact your admin for help'
            ], 401);
        } else {
            return response()->json([
                'status' => 'success',
                'data' => $user,
                'msg' => 'User retrived successfully',
            ], 200);
        }
    }
}
