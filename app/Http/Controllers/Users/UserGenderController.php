<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserGenderController extends Controller
{
    /**
     * Get a list of all gender types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGenderList()
    {
        $list = User::getGender();

        return response()->json([
            'data' => $list,
            'status' => 'success',
            'msg' => 'Gender list retrieved successfully.'
        ], 200);
    }
}
