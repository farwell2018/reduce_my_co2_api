<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\OwnsVehicleRequest;
use App\Http\Requests\ProfilePictureRequest;
use App\Models\Country;
use App\Models\User;
use App\Models\Newsfeed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProfileController extends Controller
{
    /**
     * User profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile($id)
    {
        if (Auth()->id() == $id) {
            $user = User::where('id', $id)->with('country')->with('vehicles')->with('userNewsFeed')->first();
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this account kindly login and view your account'
            ], 403);
        }

        return response()->json([
            'data' => $user,
            'status' => 'success',
            'msg' => 'User profile retrieved successfully'
        ], 200);
    }

    /**
     * Update user profile
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(Request $request, $id)
    {
        if (Auth()->id() == $id) {
            $user = User::getSingleUser($id);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this account kindly login and view your account'
            ], 403);
        }

        $country = $request->input('country');
        $gender = $request->input('gender');
        $preferred_measurement = $request->input('preferred_measurement');

        if (!empty($country)) {
            $country = Country::where('name', $country)->first();
        }

        if (!empty($gender)) {
            $gender = collect(User::getGender())->where('type', $gender)->first();
        }

        if (!empty($preferred_measurement)) {
            $preferred_measurement = collect(User::getMeasurementMethod())->where('name', $preferred_measurement)->first();
        }

        $user->update([
            'gender' => isset($gender['id']) ? $gender['id'] : (int) $user->gender,
            'year_of_birth' => $request->input('year_of_birth') ? : (int) $user->year_of_birth,
            'twitter_handle' => $request->input('twitter_handle') ? : $user->twitter_handle,
            'preferred_measurement' => isset($preferred_measurement['id']) ? $preferred_measurement['id'] : (int) $user->preferred_measurement,
            'country_id' => isset($country->id) ? $country->id : (int) $user->country_id
        ]);

        if ($user->update()) {
            $user->profile_is_complete = User::PROFILE_IS_COMPLETE;
            $user->save();
        }

        return response()->json([
            'data' => $user,
            'status' => 'success',
            'msg' => 'Profile updated successfully'
        ], 200);
    }

    /**
     * Update profile image
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadProfilePic(ProfilePictureRequest $request, $id)
    {
        if (Auth()->id() == $id) {
            $user = User::getSingleUser($id);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this account kindly login and view your account'
            ], 403);
        }

        $image = $request->file('avatar');

        if (!empty($image)) {
            $avatar = uniqid().$image->getClientOriginalName();
            $avatar = strtolower(str_replace(' ', '_', $avatar));
            $image->move('images/user_profile/', $avatar);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Missing or invalid file. Try again'
            ], 422);
        }

        $user->update([
            'avatar' => $avatar
        ]);


        return response()->json([
            'status' => 'success',
            'data' => $avatar,
            'msg' => 'User profile image updated successfully'
        ], 200);
    }

    /**
     * Set if user owns a vehicle or not
     */
    public function setIfUserOwnsAVehicle(OwnsVehicleRequest $request, $userId)
    {
        if (Auth()->id() == $userId) {
            $user = User::getSingleUser($userId);
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this account kindly login and view your account'
            ], 403);
        }

        $owns_vehicle = $request->input('owns_vehicle');

        if ($owns_vehicle == 'Yes') {
            $owns_vehicle = User::OWNS_VEHICLE;
        } else {
            $owns_vehicle = User::DOES_NOT_OWN_A_VEHICLE;
        }

        $user->update([
            'owns_vehicle' => $owns_vehicle
        ]);

        return response()->json([
            'status' => 'success',
            'data' => $user,
            'msg' => 'User profile updated successfully'
        ]);
    }
}
