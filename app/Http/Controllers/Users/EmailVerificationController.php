<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Jobs\EmailVerificationJob;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmailVerificationController extends Controller
{
    /**
     * Verify User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function verify(Request $request)
    {
        $code = $request->input('verification_code');

        $user = User::where('verification_code', $code)->first();

        if (empty($user)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'User with that verification code does not exist'
            ], 404);
        }

        if (is_null($user->verified_at)) {
            $user->update([
                'verified_at' => Carbon::now(),
                'is_verified' => User::IS_VERIFIED
            ]);

            return response()->json([
                'status' => 'success',
                'msg' => 'Email verified successfully, proceed to login'
            ], 200);
        }

        return response()->json([
            'status' => 'error',
            'msg' => 'This email has already been verified.'
        ], 409);
    }

    /**
     * Resend verification code on request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendVerification(Request $request)
    {
        $email = $request->input('email');

        $user = User::where('email', $email)->first();

        if (empty($user)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'User with that email address not found'
            ], 404);
        }

        $data = [
            'name' => $user->first_name,
            'email' => $user->email,
            'code' => User::generateVerificationCode()
        ];

        EmailVerificationJob::dispatch($data);

        $user->update([
            'verification_code' => $data['code']
        ]);

        return response()->json([
            'status' => 'success',
            'msg' => 'Verification code resend, please check your email'
        ], 200);
    }
}
