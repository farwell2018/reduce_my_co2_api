<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\VehicleEcoProfileRequest;
use App\Models\Fuel;
use App\Models\Motorbike;
use App\Models\UserVehicleEcoProfile;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Auth;
use App\Models\UserVehicleUsage;

class UserVehicleEcoProfileController extends Controller
{
    /**
     * Create Eco Profile from Vehicle
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createVehicleEcoProfile(VehicleEcoProfileRequest $request)
    {
        $vehicle_type = $request->input('vehicle_type');

        $fuel_type = $request->input('vehicle_fuel_type');

        $fuel_per_week = $request->input('fuel_per_week');

        $vehicle_name = $request->input('vehicle_name');

        $motorbike_type = $request->input('motorbike_type');
        
        if (!empty($vehicle_type)) {
            $vehicle_type = Vehicle::where('vehicle_name', $vehicle_type)->first();

            if (empty($vehicle_type)) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Invalid Vehicle Type. Please Try again'
                ], 404);
            }
        }

        $fuel_type = Fuel::where('fuel_name', $fuel_type)->first();

        if (empty($fuel_type)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Invalid Fuel Type. Please Try again'
            ], 404);
        }



        if (!empty($motorbike_type)) {
            $motorbike_type = Motorbike::where('type', $motorbike_type)->first();

            if (empty($motorbike_type)) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Invalid Motorbike Type. Please Try again'
                ], 404);
            }
        }

        $vehicleProfile = UserVehicleEcoProfile::firstOrCreate([
            'user_id' => Auth::user()->id,
            'vehicle_name' => $vehicle_name,
            'vehicle_type' => isset($vehicle_type->id) ? $vehicle_type->id : null,
            'vehicle_fuel_type' => $fuel_type->id,
            'fuel_per_week' => $fuel_per_week,
            'motorbike_type' => isset($motorbike_type->id) ? $motorbike_type->id : null
        ]);
            $vehicle_usage = UserVehicleUsage::firstOrCreate([
            'user_id' =>Auth::user()->id,
            'user_vehicle_eco_profile_id' => UserVehicleEcoProfile::where('vehicle_name', $vehicle_name)->first()->id,
            'vehicle_name' => $vehicle_name,
            'usage_type' => UserVehicleUsage::JOURNEY_USAGE

            ]);


        $vehicleProfile->save();
        $vehicle_usage->save();

        return response()->json([
            'data' => $vehicleProfile,
            'status' => 'success',
            'msg' => 'Vehicle Eco Profile Created successfully'
        ], 201);
    }
    // Update vehicle_name, fuel_type, fuel_per_week
    public function updateSingleVehicle(VehicleEcoProfileRequest $request)
    {
        $vehicle_name = $request->input('vehicle_name');
        $fuel_type = $request->input('fuel_type');
        $fuel_per_week = $request->input('fuel_per_week');
        $vehicle_type = $request->input('vehicle_type');
        $motorbike_type = $request->input('motorbike_type');
        $vehicle_id = $request->input('vehicle_id');
        $vehicle_usage_id = $request->input('vehicle_usage_id');
        $vehicle_usage = UserVehicleUsage::find($vehicle_usage_id);
        $vehicle_usage->vehicle_name = $vehicle_name;
        $vehicle_usage->save();
        $vehicle_profile = UserVehicleEcoProfile::find($vehicle_id);
        $vehicle_profile->vehicle_name = $vehicle_name;
        $vehicle_profile->vehicle_type = $vehicle_type;
        $vehicle_profile->vehicle_fuel_type = $fuel_type;
        $vehicle_profile->fuel_per_week = $fuel_per_week;
        $vehicle_profile->motorbike_type = $motorbike_type;
        $vehicle_profile->save();
        return response()->json([
            'data' => $vehicle_profile,
            'status' => 'success',
            'msg' => 'Vehicle Eco Profile Updated successfully'
        ], 201);
    }

    
    // get single vehicle eco profile
    public function getVehicleEcoProfile($id)
    {
        $vehicleProfile = UserVehicleEcoProfile::where('user_id', Auth::user()->id)->where('id', $id)->first();

        if (empty($vehicleProfile)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Invalid Vehicle Eco Profile. Please Try again'
            ], 404);
        }

        return response()->json([
            'data' => $vehicleProfile,
            'status' => 'success',
            'msg' => 'Vehicle Eco Profile Fetched successfully'
        ], 200);
    }
}
