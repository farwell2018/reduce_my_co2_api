<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserCommuteRequest;
use App\Http\Requests\UserJourneyRequest;
use App\Models\Journey;
use App\Models\UserVehicleEcoProfile;
use App\Models\UserVehicleUsage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserVehicleUsageController extends Controller
{
    /**
     * Create Vehicle Usage
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createVehicleCommuteUsage(UserCommuteRequest $request)
    {
        $user = Auth::user();

        $vehicle_name = $request->input('vehicle_name');

        if (!empty($vehicle_name)) {
            $vehicle_name = UserVehicleEcoProfile::where('vehicle_name', $vehicle_name)
                ->where('user_id', $user->id)->first();
            if (empty($vehicle_name)) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Missing or Invalid Vehicle name. Try again'
                ], 404);
            }
        }

        $vehicle_usage = UserVehicleUsage::firstOrCreate([
            'user_id' => $user->id,
            'user_vehicle_eco_profile_id' => $vehicle_name->id,
            'vehicle_name' => $vehicle_name->vehicle_name,
            'usage_type' => UserVehicleUsage::COMMUTE_USAGE
        ]);

        if ($vehicle_usage->save()) {
            $usage_measurements = [
                'days_per_week' => $request->input('days_per_week'),
                'distance_per_day' => $request->input('distance_per_day')
            ];

            $usage_measurements = [
                'days_per_week' => $usage_measurements['days_per_week'],
                'distance_per_day' => $usage_measurements['distance_per_day'] * 2,
                'distance_per_week' => ($usage_measurements['distance_per_day']) * 2 * $usage_measurements['days_per_week']
            ];
        }

        $vehicle_usage->update($usage_measurements);

        return response()->json([
            'data' => $vehicle_usage,
            'status' => 'success',
            'msg' => 'User vehicle usage created successfully'
        ], 201);
    }

    /**
     * Get single user commmute
     */
    public function getSingleCommute($commuteId)
    {
        $commute = UserVehicleUsage::where('usage_type', UserVehicleUsage::COMMUTE_USAGE)
                ->where('id', $commuteId)->where('user_id', Auth::id())->first();

        return response()->json([
            'data' => $commute,
            'status' => 'success',
            'msg' => 'Commute retrieved successfully'
        ], 200);
    }

    /**
     * Create Vehicle Journey Usage
     */
    public function createVehicleJourneyUsage(UserJourneyRequest $request)
    {
        $user_id = Auth::id();

        $vehicle = $request->input('vehicle_name');
        $journey = $request->input('journey');

        if (!empty($vehicle)) {
            $vehicle_profile = UserVehicleEcoProfile::where('vehicle_name', $vehicle)
            ->where('user_id', $user_id)->first();
            if (empty($vehicle_profile)) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Missing or Invalid Vehicle name. Try again'
                ], 404);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Missing or Invalid Vehicle name. Try again'
            ], 404);
        }

        if (!empty($journey)) {
            $journey = Journey::where('name', $journey)->first();
            if (empty($journey)) {
                return response()->json([
                    'status' => 'error',
                    'msg' => 'Missing or invalid Journey. Please try again'
                ], 404);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'msg' => 'Missing or invalid Journey. Please try again'
            ], 404);
        }

        $commute = UserVehicleUsage::firstOrCreate([
            'user_id' => $user_id,
            'user_vehicle_eco_profile_id' => $vehicle_profile->id,
            'vehicle_name' => $vehicle_profile->vehicle_name,
            'usage_type_id' => $journey->id,
            'usage_type' => UserVehicleUsage::JOURNEY_USAGE
        ]);

        if ($commute->save()) {
            $commute_measurements = [
                'days_per_week' => $request->input('days_per_week'),
                'distance_per_day' => $request->input('one_way_distance')
            ];

            $commute_measurements = [
                'days_per_week' => $commute_measurements['days_per_week'],
                'distance_per_day' => $commute_measurements['distance_per_day'] * 2,
                'distance_per_week' => ($commute_measurements['distance_per_day']) * 2 * $commute_measurements['days_per_week']
            ];
        }

        $commute->update($commute_measurements);

        return response()->json([
            'status' => 'success',
            'data' => $commute,
            'msg' => 'Commute created successfully'
        ], 201);
    }

    /**
     * Get single user commmute
     */
    public function getSingleJourney($journeyId)
    {
        $commute = UserVehicleUsage::where('usage_type', UserVehicleUsage::JOURNEY_USAGE)
                ->where('id', $journeyId)->where('user_id', Auth::id())->first();

        return response()->json([
            'data' => $commute,
            'status' => 'success',
            'msg' => 'Journey retrieved successfully'
        ], 200);
    }
}
