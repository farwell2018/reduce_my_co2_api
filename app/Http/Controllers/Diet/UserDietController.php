<?php

namespace App\Http\Controllers\Diet;

use App\Http\Controllers\Controller;
use App\Models\DietFootPrintFactor;
use App\Models\UserDiet;
use App\Models\UserDietTotalEmission;
use App\Models\UserHomeDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserDietController extends Controller
{
    /**
     * Get user diet details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function dietDetails()
    {
        $data = [
            'diet_preferences' => $this->preferences(),
            'knows_calories_i_eat' => $this->yesOrNo()
        ];

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'msg' => 'Diet details retrieved successfully'
        ], 200);
    }

    /**
     * Save user diet details
     */
    public function storeDietDetails(Request $request)
    {
        $user = Auth::user();
        $lamb = $request->input('lamb');
        $beef = $request->input('beef');
        $prawns = $request->input('prawns');
        $fish = $request->input('fish');
        $chicken = $request->input('chicken');
        $cheese = $request->input('cheese');
        $milk = $request->input('milk');
        $eggs = $request->input('eggs');
        $beans = $request->input('beans');
        $pork = $request->input('pork');
        $people = $request->input('people');
        $user_diet_preference = $request->input('user_diet_preference');
        $knows_calories_i_consume = $request->input('knows_calories_i_consume');
        $calories = $request->input('calories');
        $weight = $request->input('weight');
        $height = $request->input('height');

        // if (!empty($user_diet_preference)) {
        //     $user_diet_preference = collect($this->preferences())->where('type', $user_diet_preference)->first();
        // } else {
        //     return response()->json([
        //         'status' => 'error',
        //         'msg' => 'Missing or Invalid diet type. Try again'
        //     ], 404);
        // }

        // if (!empty($knows_calories_i_consume)) {
        //     $knows_calories_i_consume = collect($this->yesOrNo())->where('type', $knows_calories_i_consume)->first();
        // } else {
        //     return response()->json([
        //         'status' => 'error',
        //         'msg' => 'Missing or Invalid Response. Try again'
        //     ], 404);
        // }

        if ($knows_calories_i_consume['type'] == 'No') {
            $calories = estimateCalories($user, $weight, $height);
        }

        $details = UserDiet::firstOrCreate([
            'user_id' => Auth::id(),
            'user_diet_preference' => $user_diet_preference['id'],
            'lamb' => $lamb,
            'beef' => $beef,
            'prawns' => $prawns,
            'fish' => $fish,
            'chicken' => $chicken,
            'cheese' => $cheese,
            'milk' => $milk,
            'eggs' => $eggs,
            'beans' => $beans,
            'pork' => $pork,
            'people' => $people,

        ]);

        if ($details->save()) {
            $details->update([
                'knows_calories_i_consume' => $knows_calories_i_consume['id'],
                'calories' => $calories,
                'weight' => $weight,
                'height' => $height
            ]);
        }

        return response()->json([
            'data' => $details,
            'status' => 'success',
            'msg' => 'Diet details created successfully'
        ], 201);
    }

    /**
     * Get User Diet Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDietDetails($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $dietUsageDetails = UserDiet::where('user_id', $userId)->get();
        // dd($dietUsageDetails);
        $dietUsageDetails = $dietUsageDetails->map(function ($details) {
            $diet_preference = collect($this->preferences())->where('id', $details->user_diet_preference)->first();
            return [
                'diet_preference' => $diet_preference['type'],
                'id' => $details->id,
                'calories' => $details->calories,
                'weight' => $details->weight,
                'height' => $details->height,
                'people' => $details->people,
                'lamb' => $details->lamb,
                'beef' => $details->beef,
                'prawns' => $details->prawns,
                'fish' => $details->fish,
                'chicken' => $details->chicken,
                'cheese' => $details->cheese,
                'milk' => $details->milk,
                'eggs' => $details->eggs,
                'beans' => $details->beans,
                'pork' => $details->pork,
                

            ];
        });

        return response()->json([
            'data' => $dietUsageDetails,
            'status' => 'success',
            'msg' => 'Diet details retrieved successfully'
        ], 200);
    }

    /**
     * Calculate user diet emission
     */
    public function userDietEmission($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $dietDetails = $this->calculate($userId)->sum();

        $this->saveDietEmission($dietDetails, $userId);

        return response()->json([
            'data' => $dietDetails . 't',
            'status' => 'success',
            'msg' => 'Diet footprint calculated successfully'
        ], 201);
    }

    /**
     * Get Factor and Return Calculation
     */
    public function calculate($userId)
    {
        return UserDiet::where('user_id', $userId)->get()->map(function ($details) {
            // dd($details);
            $dietFactor = DietFootPrintFactor::all();
            //get emission of food_type from diet_footprint_factor table
            $dietFactors = $dietFactor->map(function ($factor) {
                return [
                    'food_type' => $factor->food_type,
                    'total_emissions' => $factor->total_emissions,
                ];
                // return $factor->emission;
            });

            // dd($dietFactors);



            // dd($dietFactor);
            // if ($details->calories) {
            //     $emission = round(
            //         ($details->calories * config('reduce.calories.average_emission')) / config('reduce.calories.average_calories'),
            //         config('reduce.calculations.round_of_to')
            //     );
            // } else {
            if (!empty($dietFactor)) {
                // $emission = round($dietFactor->conversion_factor, config('reduce.calculations.round_of_to'));
                //calculate total emissions based on sum($details * $dietFactors)
                $emission = round(
                    ( ($details->lamb * $dietFactors[0]['total_emissions']) +
                    ($details->beef * $dietFactors[1]['total_emissions']) +
                    ($details->prawns * $dietFactors[2]['total_emissions']) +
                    ($details->fish * $dietFactors[3]['total_emissions']) +
                    ($details->chicken * $dietFactors[4]['total_emissions']) +
                    ($details->cheese * $dietFactors[5]['total_emissions']) +
                    ($details->milk * $dietFactors[6]['total_emissions']) +
                    ($details->eggs * $dietFactors[7]['total_emissions']) +
                    ($details->beans * $dietFactors[8]['total_emissions']) +
                    ($details->pork * $dietFactors[9]['total_emissions'])
                    ) * $details->people,
                    config('reduce.calculations.round_of_to')
                );
            } else {
                $emission = 0;
            }
            // }

            return $emission;
            dd($emission);
        });
    }

    /**
     * Update User Diet Usage
     */
    public function updateDietUsage(Request $request, $usageId)
    {
        $userDiet = $this->singleDietUsage($usageId);

        $user_diet_preference = $request->input('user_diet_preference');
        $knows_calories_i_consume = $request->input('knows_calories_i_consume');

        if (!empty($user_diet_preference)) {
            $user_diet_preference = collect($this->preferences())->where('type', $user_diet_preference)->first();
        }

        if (!empty($knows_calories_i_consume)) {
            $knows_calories_i_consume = collect($this->yesOrNo())->where('type', $knows_calories_i_consume)->first();
        }

        $userDiet->update([
            // 'user_diet_preference' => isset($user_diet_preference['id']) ? $user_diet_preference['id'] : $userDiet->user_diet_preference,
            // 'knows_calories_i_consume' => isset($knows_calories_i_consume['id']) ? $knows_calories_i_consume['id'] : $userDiet->knows_calories_i_consume,
            // 'calories' => $request->input('calories') ?: $userDiet->calories,
            // 'weight' => $request->input('weight') ?: $userDiet->weight,
            // 'height' => $request->input('height') ?: $userDiet->height,
            'people' => $request->input('people') ?: $userDiet->people,
            'lamb' => $request->input('lamb') ?: $userDiet->lamb,
            'beef' => $request->input('beef') ?: $userDiet->beef,
            'prawns' => $request->input('prawns') ?: $userDiet->prawns,
            'fish' => $request->input('fish') ?: $userDiet->fish,
            'chicken' => $request->input('chicken') ?: $userDiet->chicken,
            'cheese' => $request->input('cheese') ?: $userDiet->cheese,
            'milk' => $request->input('milk') ?: $userDiet->milk,
            'eggs' => $request->input('eggs') ?: $userDiet->eggs,
            'beans' => $request->input('beans') ?: $userDiet->beans,
            'pork' => $request->input('pork') ?: $userDiet->pork,

        ]);

        return response()->json([
            'data' => $userDiet,
            'status' => 'success',
            'msg' => 'User Diet updated successfully'
        ], 200);
    }

    /**
     * Get single user diet usage
     */
    public function getSingleDietUsage($usageId)
    {
        $userDiet = $this->singleDietUsage($usageId);

        return response()->json([
            'data' => $userDiet,
            'status' => 'success',
            'msg' => 'User diet retrieved successfully'
        ], 200);
    }

    /**
     * Save user diet emission
     */
    public function saveDietEmission($emission, $userId)
    {
        $emissions = UserDietTotalEmission::firstOrCreate([
            'user_id' => $userId
        ]);

        if ($emissions->save()) {
            $emissions->update([
                'total_emissions' => $emission
            ]);
        }

        return $emissions;
    }

    /**
     * Retrieve diet usage
     */
    public function singleDietUsage($id)
    {
        return UserDiet::where('id', $id)->first();
    }


    /**
     * Diet Preferences
     */
    public function preferences(): array
    {
        return UserDiet::dietPreferences();
    }

    /**
     * Get Yes Or No Answer
     */
    public function yesOrNo(): array
    {
        return UserHomeDetail::getYesOrNo();
    }
}
