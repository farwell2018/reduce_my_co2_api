<?php

namespace App\Http\Controllers\Motorbikes;

use App\Http\Controllers\Controller;
use App\Models\Motorbike;
use Illuminate\Http\Request;

class MotorbikeController extends Controller
{
    /**
     * Get all motorbikes
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMotorbikes()
    {
        $motorbikes = Motorbike::where('status', Motorbike::ACTIVE_MOTORBIKE)->get();

        return response()->json([
            'data' => $motorbikes,
            'status' => 'success',
            'msg' => 'Motorbikes retrieved successfully'
        ], 200);
    }
}
