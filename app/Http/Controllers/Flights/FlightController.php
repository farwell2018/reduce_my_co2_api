<?php

namespace App\Http\Controllers\Flights;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateFlightUsageRequest;
use App\Models\Flight;
use App\Models\FlightClass;
use App\Models\FlightUsage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FlightController extends Controller
{
    /**
     * Get All Flight Types
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFlightTypes()
    {
        $flights = Flight::where('status', Flight::ACTIVE_FLIGHT_TYPE)->get();

        return response()->json([
            'data' => $flights,
            'status' => 'success',
            'msg' => 'Flights retrieved successfully'
        ], 200);
    }

    /**
     * Get All Flight Types
     */
    public function getFlightClasses()
    {
        $classes = FlightClass::where('status', FlightClass::ACTIVE_FLIGHT_CLASS)->latest()->get();

        return response()->json([
            'data' => $classes,
            'status' => 'success',
            'msg' => 'Flights classes retrieved successfully'
        ], 200);
    }
    /**
     * Get All Commercial Plane data
     */

    public function getCommercialPlanes()
    {
        $planes = DB::table('commercial_flights')->get();

        return response()->json([
            'data' => $planes,
            'status' => 'success',
            'msg' => 'Commercial planes retrieved successfully'
        ], 200);
    }
        /**
     * Get All Private Plane data
     */

    public function getPrivatePlanes()
    {
        $planes = DB::table('private_flights')->get();

        return response()->json([
            'data' => $planes,
            'status' => 'success',
            'msg' => 'Private planes retrieved successfully'
        ], 200);
    }
    /**
     * Add Flight Usage
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createFlightUsage(CreateFlightUsageRequest $request)
    {
        $user = Auth::user();

        $flight_type = $request->input('flight_type');
        $flight_class = $request->input('flight_class');
        $number_of_passengers = $request->input('number_of_passengers');
        $distance = $request->input('distance');
        $one_way_or_round = $request->input('one_way_or_round');
        $destination = $request->input('destination');
        $purpose = $request->input('purpose');
        $model = $request->input('model');
        $engine_type = $request->input('engine_type');
        $duration = $request->input('duration');


        $flight_type = Flight::where('flight_type', $flight_type)->first();

        if (empty($flight_type)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Missing or invalid flight type. Try again.'
            ], 404);
        }
        
        $flight_class = FlightClass::where('flight_class_name', $flight_class)->first();
        if (empty($flight_class)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Missing or invalid flight class. Try again.'
            ], 404);
        }

        if ($one_way_or_round == FlightUsage::ONE_WAY) {
            $distance = $distance;
        } elseif ($one_way_or_round == FlightUsage::ROUND) {
            $distance = $distance * 2;
        }

        $flightUsage = FlightUsage::firstOrCreate([
            'user_id' => $user->id,
            'flight_type' => $flight_type->id,
            'flight_class' => $flight_class->id,
            'model' => $model,
            'engine_type' => $engine_type,
            'duration' => $duration
        ]);

        if ($flightUsage->save()) {
            $flightUsage->update([
                'distance' => $distance,
                'number_of_passengers' => $number_of_passengers,
                'one_way_or_round' => $one_way_or_round,
                'destination' => $destination,
                'purpose' => $purpose,
                'model' => $model,
                'engine_type' => $engine_type,
                'duration' => $duration
            ]);
        }

        return response()->json([
            'data' => $flightUsage,
            'status' => 'success',
            'msg' => 'Flight usage created successfully'
        ], 200);
    }

    /**
     * Get User Flights
     */
    public function getUserFlights($userId)
    {
        if (Auth()->id() != $userId) {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this account kindly login and view your account'
            ], 403);
        }

        $flights = $this->userFlights($userId);

        return response()->json([
            'data' => $flights,
            'status' => 'success',
            'msg' => 'User flights retrieved successfully'
        ], 200);
    }

    /**
     * User Flights
     */
    public function userFlights($userId)
    {
        $flightsTable = (new Flight())->getTable();
        $flightClassesTable = (new FlightClass())->getTable();
        $flightUsageTable = (new FlightUsage())->getTable();

        $flights = DB::table($flightUsageTable)
                ->where('user_id', $userId)
                ->join($flightClassesTable, $flightUsageTable . '.flight_class', '=', $flightClassesTable . '.id')
                ->join($flightsTable, $flightUsageTable . '.flight_type', '=', $flightsTable . '.id')
                ->select([
                    $flightsTable . '.flight_type',
                    $flightUsageTable . '.purpose',
                    $flightUsageTable . '.id',
                    $flightUsageTable . '.model',
                    $flightUsageTable . '.duration',
                    $flightUsageTable . '.engine_type',
                    $flightClassesTable . '.flight_class_name'
                ])
                ->get();

        return $flights;
    }

    /**
     * Get single created flight journey
     */
    public function getUserFlight($flightId)
    {
        $flight = $this->getSingleUserFlight($flightId);

        return response()->json([
            'data' => $flight,
            'status' => 'success',
            'msg' => 'User flight retrieved successfully'
        ], 200);
    }

    /**
     * Edit user flights
     */
    public function updateFlight(Request $request, $userId, $flightId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $userFlight = $this->getSingleUserFlight($flightId);

        $flight = $request->input('flight');

        $flight_class = $request->input('flight_class');

        if (!empty($flight)) {
            $flight_type = Flight::where('flight_type', $flight)->first();
        }

        if (!empty($flight_class)) {
            $flight_class = FlightClass::where('flight_class_name', $flight_class)->first();
        }

        $userFlight->update([
            'number_of_passengers' => $request->input('number_of_passengers') ? : $userFlight->number_of_passengers,
            'purpose' => $request->input('purpose') ? : $userFlight->purpose,
            'destination' => $request->input('destination') ? : $userFlight->destination,
            'distance' => $request->input('distance') ? : $userFlight->distance,
            'flight_type' => isset($flight_type->id) ? $flight_type->id : $userFlight->flight_type,
            'flight_class_name' => isset($flight_class->id) ? $flight_class->id : $userFlight->flight_class_name,
            'model' => $request->input('model') ? : $userFlight->model,
            'engine_type' => $request->input('engine_type') ? : $userFlight->engine_type,
            'duration' => $request->input('duration') ? : $userFlight->duration
        ]);

        return response()->json([
            'data' => $userFlight,
            'status' => 'success',
            'msg' => 'Flight updated successfully'
        ], 200);
    }

    /**
     * Get user flight
     */
    public function getSingleUserFlight($flightId)
    {
        return FlightUsage::where('id', $flightId)->first();
    }
}
