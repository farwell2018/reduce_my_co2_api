<?php

namespace App\Http\Controllers\Flights;

use App\Http\Controllers\Controller;
use App\Models\CommercialFlight;
use App\Models\Flight;
use App\Models\FlightConversionFactor;
use App\Models\FlightUsage;
use App\Models\PrivateFlight;
use App\Models\UserFlightsTotalEmission;
use Illuminate\Http\Request;

class FlightEmissionsController extends Controller
{
    /**
     * Calculate Flight Emissions
     *
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function calclulateFlightEmission($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $userFlights = FlightUsage::where('user_id', $userId)->get();
        
        $emission = $userFlights->map(function ($flight) {
            $factor = $this->getMatchingFactorBasedOnFlight($flight);
            // dd($flight);
            if (!empty($factor)) {
                $converted = ($flight->duration * $factor->emissions_per_passenger_hour) * $flight->number_of_passengers;
            } else {
                $converted = 0;
            }

            return ($converted * Flight::EQUIVALENT_OF_ONE_TONNE);
        })->sum();

        $this->saveFlightTotalEmission($userId, $emission);

        return response()->json([
            'data' => round($emission, Flight::ROUND_OF_TO),
            'status' => 'success',
            'msg' => 'Flight emission calculated successfully'
        ], 200);
    }

    /**
     * Get Matching Factor based on Flight and Class
     * @param object flight
     */
    public function getMatchingFactorBasedOnFlight($flight)
    {
        if ($flight->flight_type == 2) {
            $factor = CommercialFlight::where('aircraft', $flight->model)
                            ->first();
        } elseif ($flight->flight_type == 1) {
            $factor = PrivateFlight::where('aircraft', $flight->model)
                            ->first();
        }
        return $factor;
    }

    /**
     * Save Flight total emissions per user
     *
     * @param integer $userId
     * @param float|integer $emission
     *
     * @return \Illuminate\Collections
     */
    public function saveFlightTotalEmission($userId, $emission)
    {
        $emissions = UserFlightsTotalEmission::firstOrCreate([
            'user_id' => $userId
        ]);

        if ($emissions->save()) {
            $emissions->update([
                'total_emissions' => $emission
            ]);
        }

        return $emissions;
    }
}
