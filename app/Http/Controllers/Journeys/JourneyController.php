<?php

namespace App\Http\Controllers\Journeys;

use App\Http\Controllers\Controller;
use App\Models\Journey;
use App\Models\UserVehicleEcoProfile;
use App\Models\UserVehicleUsage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class JourneyController extends Controller
{
    /**
     * Get a list of all journeys
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllJourneys()
    {
        $journeys = Journey::all();

        return response()->json([
            'status' => 'success',
            'data' => $journeys,
            'msg' => 'Journeys retrived successfully'
        ], 200);
    }

    /**
     * Get User Journeys
     *
     * @param integer userId
     */
    public function getUserJourneys($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $journeys = $this->filterUserJourneys($userId);

        return response()->json([
            'data' => $journeys,
            'status' => 'success',
            'msg' => 'User journeys retrieved successfully'
        ], 200);
    }

    /**
     * Filter User Journeys
     *
     * @param integer userId
     */
    public function filterUserJourneys($userId)
    {
        $usagesTable = (new UserVehicleUsage())->getTable();

        $journeysTable = (new Journey())->getTable();

        $journeys = DB::table($usagesTable)
                ->where('user_id', $userId)
                ->where('usage_type', 'journey')
                ->join($journeysTable, $usagesTable . '.usage_type_id', '=', $journeysTable . '.id')
                ->select(
                    $usagesTable . '.id AS journey_id',
                    'vehicle_name',
                    'name AS journey_name',
                    'days_per_week',
                    'distance_per_day'
                )
                ->get();

        return $journeys;
    }

    /**
     * Update a single journey
     *
     * @param integer journeyId
     */
    public function updateUserJourney(Request $request, $journeyId)
    {
        $journey = $request->input('journey');
        $days_per_week = $request->input('days_per_week');
        $distance_per_day = $request->input('one_way_distance');

        $journeyToUpdate = UserVehicleUsage::where('id', $journeyId)
                        ->where('usage_type', UserVehicleUsage::JOURNEY_USAGE)
                        ->first();

        if (!empty($journey)) {
            $journey = Journey::where('name', $journey)->first();
        }

        if (!empty($distance_per_day) || !empty($days_per_week)) {
            $daysPerWeek = $days_per_week;
            $distancePerDay = $distance_per_day * 2;
        }else{
            $daysPerWeek = (string) $journeyToUpdate->days_per_week;
            $distancePerDay = $journeyToUpdate->distance_per_day;
        }

        if (!empty($distance_per_day) && !empty($days_per_week)) {
            $distancePerWeek = $distancePerDay * 2 * $days_per_week;
        }else{
            $distancePerWeek = $journeyToUpdate->distance_per_week;
        }

        $journeyToUpdate->update([
            'usage_type_id' => isset($journey->id) ? $journey->id : $journeyToUpdate->usage_type_id,
            'days_per_week' => $daysPerWeek ? $daysPerWeek : (string) $journeyToUpdate->days_per_week,
            'distance_per_day' => $distancePerDay ? $distancePerDay : $journeyToUpdate->distance_per_day,
            'distance_per_week' => $distancePerWeek
        ]);

        return response()->json([
            'data' => $journeyToUpdate,
            'status' => 'success',
            'msg' => 'User journey updated successfully'
        ], 200);
    }
}
