<?php

namespace App\Http\Controllers\Commutes;

use App\Http\Controllers\Controller;
use App\Models\UserVehicleEcoProfile;
use App\Models\UserVehicleUsage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommutesController extends Controller
{
    /**
     * Filter User Journeys
     *
     * @param integer userId
     */
    public function filterUserCommutes($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $commutes = UserVehicleUsage::where('user_id', $userId)
                ->where('usage_type', 'commute')
                ->select('id', 'vehicle_name', 'distance_per_week', 'days_per_week')
                ->get();

        return response()->json([
            'data' => $commutes,
            'status' => 'success',
            'msg' => 'User commutes retrieved successfully'
        ], 200);
    }

    /**
     * Update a single commute
     *
     * @param integer commuteId
     */
    public function updateUserCommute(Request $request, $commuteId)
    {
        $vehicle_name = $request->input('vehicle_name');
        $days_per_week = $request->input('days_per_week');
        $distance_per_day = $request->input('distance_per_day');

        $commuteToUpdate = UserVehicleUsage::where('id', $commuteId)
                        ->where('usage_type', UserVehicleUsage::COMMUTE_USAGE)
                        ->first();

        if (!empty($distance_per_day) || !empty($days_per_week)) {
            $daysPerWeek = $days_per_week;
            $distancePerDay = $distance_per_day * 2;
        }else{
            $daysPerWeek = (string) $commuteToUpdate->days_per_week;
            $distancePerDay = $commuteToUpdate->distance_per_day;
        }

        if (!empty($distance_per_day) && !empty($days_per_week)) {
            $distancePerWeek = $distancePerDay * 2 * $days_per_week;
        }else{
            $distancePerWeek = $commuteToUpdate->distance_per_week;
        }

        $commuteToUpdate->update([
            'days_per_week' => $daysPerWeek ? $daysPerWeek : (string) $commuteToUpdate->days_per_week,
            'distance_per_day' => $distancePerDay ? $distancePerDay : $commuteToUpdate->distance_per_day,
            'distance_per_week' => $distancePerWeek ? $distancePerWeek : $commuteToUpdate->distance_per_week
        ]);

        return response()->json([
            'data' => $commuteToUpdate,
            'status' => 'success',
            'msg' => 'User journey updated successfully'
        ], 200);
    }
}
