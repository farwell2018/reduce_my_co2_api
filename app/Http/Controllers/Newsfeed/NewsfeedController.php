<?php

namespace App\Http\Controllers\Newsfeed;

use App\Http\Controllers\Controller;
use App\Models\Newsfeed;
use App\Models\UserNewsFeed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewsfeedController extends Controller
{
    public function newsfeed()
    {

        $newsfeed = $this->getNewsfeed();
        // dd($newsfeed);
        return response()->json([
    
            'data' => $newsfeed,
            'status' => 'success',
            'msg' => 'Newsfeeds retrieved successfully.'
        ], 200);
    }



    /**
     * Get news
     */
    public function getNewsfeed()
    {
        return Newsfeed::where('status', Newsfeed::ACTIVE_FEED)->get();
    }
    /**
     * Get users news
     */

    // public function getUserNews($user_id)
    // {
    //     $user = Auth::user();
    //     // dd($user);
    //     if ($user->id != $user_id) {
    //         return $this->errorResponse('You do not have access to this account!', 403);
    //     }
    //     return Newsfeed::where('user_id', $user_id)->get();
    // }

        /**
     * Get User Journeys
     *
     * @param integer userId
     */
    public function getUserNewsFeed($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $newsfeed = $this->filterUserNewsFeeds($userId);

        return response()->json([
            'data' => $newsfeed,
            'status' => 'success',
            'msg' => 'User newsfeed retrieved successfully'
        ], 200);
    }

    /**
     * Filter User newsfeed
     *
     * @param integer userId
     */
    public function filterUserNewsFeeds($userId)
    {
        $newsfeedTable = (new UserNewsFeed())->getTable();

        $userNewsFeedTable = (new NewsFeed())->getTable();

        $newsfeeds = DB::table($newsfeedTable)
                ->where('user_id', $userId)
                ->join($userNewsFeedTable, $newsfeedTable . '.newsfeed', '=', $userNewsFeedTable . '.id')
                ->select(
                    $newsfeedTable . '.id AS newsfeed_id',
                    'title',
                    'url',

                )
                ->get();

        return $newsfeeds;
    }
    /**
     * Post news
     */
    public function createNewsfeed(Request $request)
    {
        $user = Auth::user();
        $title = $request->input('title');
        $url = $request->input('url');
    



        if (empty($title)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Missing or invalid newsfeed. Try again.'
            ], 404);
        }

        if (empty($url)) {
            return response()->json([
                'status' => 'error',
                'msg' => 'Missing or invalid Url. Try again.'
            ], 404);
        }


        $newsfeed = Newsfeed::firstOrCreate([
            'title' => $title,
            'url' => $url
        ]);

        if ($newsfeed->save()) {
            $newsfeed->update([
                'title' => $title,
                'url' => $url
            ]);
        }

        return response()->json([
            'data' => $newsfeed,
            'status' => 'success',
            'msg' => 'Newsfeed  created successfully'
        ], 200);
    }
}
