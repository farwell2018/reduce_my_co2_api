<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\HouseHoldFactor;
use App\Models\UserHomeDetail;
use App\Models\UserHouseHoldTotalEmission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Get Houses
     */
    public function getHouseDetails()
    {
        $data = [
            'houses' => $this->houses(),
            'sizes' => $this->houseSizes(),
            'answers' => $this->yesOrNo(),
            'heats' => $this->houseHeats()
        ];

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'msg' => 'Homes retrieved successfully'
        ], 200);
    }

    /**
     * Create User Home Details
     *
     * @param integer userId
     */
    public function storeUserHomeDetails(Request $request)
    {
        // $lives_in = $request->input('lives_in');
        // $home_size = $request->input('home_size');
        $no_of_shared_people = $request->input('no_of_shared_people');
        // $knows_electricity_consumption = $request->input('knows_electricity_consumption');
        $electricity_consumption = $request->input('electricity_consumption');
        // $heat_consumption_amount = $request->input('heat_consumption_amount');
        // $knows_water_consumption = $request->input('knows_water_consumption');
        // $water_consumption_amount = $request->input('water_consumption_amount');

        // if (!empty($lives_in)) {
        //     $lives_in = collect($this->houses())->where('type', $lives_in)->first();
        // } else {
        //     return response()->json([
        //         'status' => 'error',
        //         'msg' => 'Missing or Invalid home type. Try again'
        //     ], 404);
        // }

        // if (!empty($home_size)) {
        //     $home_size = collect($this->houseSizes())->where('type', $home_size)->first();
        // } else {
        //     return response()->json([
        //         'status' => 'error',
        //         'msg' => 'Missing or Invalid home size. Try again'
        //     ], 404);
        // }

        // if (!empty($knows_electricity_consumption)) {
        //     $knows_electricity_consumption = collect($this->yesOrNo())->where('type', $knows_electricity_consumption)->first();
        // } else {
        //     return response()->json([
        //         'status' => 'error',
        //         'msg' => 'Missing or Invalid Response. Try again'
        //     ], 404);
        // }

        // if (!empty($knows_water_consumption)) {
        //     $knows_water_consumption = collect($this->yesOrNo())->where('type', $knows_water_consumption)->first();
        // } else {
        //     return response()->json([
        //         'status' => 'error',
        //         'msg' => 'Missing or Invalid Response. Try again'
        //     ], 404);
        // }

        // if ($knows_electricity_consumption['type'] == 'No') {
        //     $electricity_consumption = estimatePowerUsage($no_of_shared_people);
        // }

        $details = UserHomeDetail::firstOrCreate([
            'user_id' => Auth::id(),
            // 'lives_in' => $lives_in['id'],
            // 'home_size' => $home_size['id'],
            'no_of_shared_people' => $no_of_shared_people
        ]);

        if ($details->save()) {
            $details->update([
                // 'knows_electricity_consumption' => $knows_electricity_consumption['id'],
                'electricity_consumption' => $electricity_consumption,
                // 'heat_consumption_amount' => $heat_consumption_amount,
                // 'knows_water_consumption' => $knows_water_consumption['id'],
                // 'water_consumption_amount' => $water_consumption_amount,
            ]);
        }

        return response()->json([
            'data' => $details,
            'status' => 'success',
            'msg' => 'User home details saved successfully'
        ], 201);
    }

    /**
     * Get User House Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserHouseDetails($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $houseUsageDetails = UserHomeDetail::where('user_id', $userId)->get();

        $houseUsageDetails = $houseUsageDetails->map(function ($details) {
            $lives_in = collect($this->houses())->where('id', $details->lives_in)->first();
            $home_size = collect($this->houseSizes())->where('id', $details->home_size)->first();

            return [
                'id' => $details->id,
                'lives_in' => $lives_in['type'],
                'home_size' => $home_size['type'],
                'no_of_shared_people' => $details->no_of_shared_people,
                'electricity_consumption' => $details->electricity_consumption,
                'heat_consumption_amount' => $details->heat_consumption_amount,
                'water_consumption_amount' => $details->water_consumption_amount
            ];
        });

        return response()->json([
            'data' => $houseUsageDetails,
            'status' => 'success',
            'msg' => 'Home details retrieved successfully'
        ], 200);
    }

    /**
     * Calculate the household factors
     */
    public function calculateHouseHoldFactors($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $calculated = $this->calculate($userId)->sum();

        $calculated = round($calculated, config('reduce.calculations.round_of_to'));

        $this->storeEmission($calculated, $userId);

        return response()->json([
            'data' => $calculated,
            'status' => 'success',
            'msg' => 'House hold emission calculated successfully'
        ], 201);
    }

    /**
     * Get factor and calculate the emission
     */
    public function calculate($userId)
    {
        $houseHoldDetails = UserHomeDetail::where('user_id', $userId)->get();

        return $houseHoldDetails->map(function ($details) use ($userId) {
            $householdFactor = HouseHoldFactor::first();
            $kwh = $details->electricity_consumption / 22;

            $electricity = $kwh * $householdFactor->electricity * 0.001;

            // $landfill = 0.001*($details->no_of_shared_people * 33) * $householdFactor->landfill;
            // $landfill = 0.001 * ($details->no_of_shared_people  * $householdFactor->landfill);
            $total_emission = $electricity;
         
            return $total_emission;
        });
    }

    /**
     * Store the calculated emission
     */
    public function storeEmission($emissions, $userId)
    {
        $emission = UserHouseHoldTotalEmission::firstOrCreate([
            'user_id' => $userId
        ]);

        if ($emission->save()) {
            $emission->update([
                'total_emissions' => $emissions
            ]);
        }

        return $emission;
    }

    /**
     * Update user household details
     */
    public function updateHouseHoldUsage(Request $request, $usageId)
    {
        $usage = $this->singleHomeDetails($usageId);

        $lives_in = $request->input('lives_in');
        $home_size = $request->input('home_size');

        $lives_in = collect($this->houses())->where('type', $lives_in)->first();
        $home_size = collect($this->houseSizes())->where('type', $home_size)->first();

        $usage->update([
            'lives_in' => isset($lives_in['id']) ? $lives_in['id'] : $usage->lives_in,
            'home_size' => isset($home_size['id']) ? $home_size['id'] : $usage->home_size,
            'no_of_shared_people' => $request->input('no_of_shared_people') ? : $usage->no_of_shared_people,
            'electricity_consumption' => $request->input('electricity_consumption') ? : $usage->electricity_consumption,
            'heat_consumption_amount' => $request->input('heat_consumption_amount') ? : $usage->heat_consumption_amount,
            'water_consumption_amount' => $request->input('water_consumption_amount') ? : $usage->water_consumption_amount
        ]);

        return response()->json([
            'data' => $usage,
            'status' => 'success',
            'msg' => 'User household details updated successfully'
        ], 200);
    }

    /**
     * Get house hold usage
     */
    public function getHouseHoldUsage($usageId)
    {
        $usage = $this->singleHomeDetails($usageId);

        return response()->json([
            'data' => $usage,
            'status' => 'success',
            'msg' => 'Home details retrieved successfully'
        ], 200);
    }

    /**
     * Get Single Home Details
     */
    public function singleHomeDetails($usageId)
    {
        return UserHomeDetail::where('id', $usageId)->first();
    }

    /**
     * Get All Houses
     */
    public function houses(): array
    {
        return UserHomeDetail::getHomes();
    }

    /**
     * Get All House Sizes
     */
    public function houseSizes(): array
    {
        return UserHomeDetail::getHouseSizes();
    }

    /**
     * Get Yes Or No Answer
     */
    public function yesOrNo(): array
    {
        return UserHomeDetail::getYesOrNo();
    }

    /**
     * Get All House Heats
     */
    public function houseHeats(): array
    {
        return UserHomeDetail::houseHeats();
    }
}
