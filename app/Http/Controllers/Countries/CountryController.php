<?php

namespace App\Http\Controllers\Countries;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function countries()
    {

        $countries = $this->getCountries();

        return response()->json([
            'data' => $countries,
            'status' => 'success',
            'msg' => 'Countries retrived successfully.'
        ], 200);
    }

    /**
     * Get country codes
     */
    public function getCountryCodes()
    {
        $countries = $this->getCountries();

        if(!empty($countries)){
            $codes = $countries->map(function($country){
                return [
                    'code' => $country->tel_code,
                    'country' => "$country->flag $country->name $country->tel_code"
                ];
            });

            return response()->json([
                'data' => $codes,
                'status' => 'success',
                'msg' => 'Country codes retrived successfully'
            ], 200);
        }else{
            return [];
        }
    }

    /**
     * Get countries
     */
    public function getCountries()
    {
        return Country::where('status', Country::ACTIVE_COUNTRY)->get();
    }
}
