<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use App\Models\MotorbikeConversionFactor;
use App\Models\User;
use App\Models\UserVehicleCarbonEmission;
use App\Models\UserVehicleEcoProfile;
use App\Models\UserVehicleTotalEmission;
use App\Models\UserVehicleUsage;
use App\Models\VehicleFootPrintFactor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CalculateVehicleEmissionsController extends Controller
{
    /**
     * Calculate the vehicle emission based on usage
     *
     * @param integer userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculateEmissionBasedOnUsage($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $usages = $this->getVehicleUsages($userId);
        
        $motorbikeEmissions = $this->calculateMotorbikesEmissions($usages);
        
        $vehicleEmissions = $this->calculateVehiclesEmissions($usages);
        
        $total = $motorbikeEmissions->sum() + $vehicleEmissions->sum();
        // dd($total);
        $this->saveUserVehiclesTotalEmissions($total, $userId);

        return response()->json([
            'data' => ['emission' => round($total, config('reduce.calculations.round_of_to')) . 't'],
            'status' => 'success',
            'msg' => 'Your Vehicles Carbon has been calculated successfully'
        ], 201);
    }

    /**
     * Get Vehicle Usages
     *
     * @param integer $userId
     */
    public function getVehicleUsages($userId)
    {
        $vehicleEcoProfileTable = (new UserVehicleEcoProfile())->getTable();

        $vehicleUsageTable = (new UserVehicleUsage())->getTable();
        $vehicles = UserVehicleEcoProfile::where('user_id', $userId)->get();
        // dd($vehicles);
        $usersTable = (new User())->getTable();

        $usages = DB::table($vehicleUsageTable)
                ->where($vehicleUsageTable . '.user_id', $userId)
                ->join($vehicleEcoProfileTable, $vehicleUsageTable . '.user_vehicle_eco_profile_id', '=', $vehicleEcoProfileTable . '.id')
                ->join($usersTable, $vehicleUsageTable . '.user_id', '=', $usersTable . '.id')
                ->select(
                    $vehicleEcoProfileTable . '.vehicle_fuel_type',
                    $vehicleEcoProfileTable . '.vehicle_type',
                    $vehicleEcoProfileTable . '.motorbike_type',
                    $vehicleEcoProfileTable . '.vehicle_name',
                    $usersTable . '.preferred_measurement',
                    $vehicleEcoProfileTable . '.fuel_per_week',
                    $vehicleEcoProfileTable . '.id AS vehicle_eco_profile_id',
                    $vehicleUsageTable . '.usage_type_id',
                    $vehicleUsageTable . '.usage_type',
                    $vehicleEcoProfileTable . '.user_id'
                )->get();
        // dd($usages);

        return $usages;
        dd($usages);
    }

    /**
     * Calculate Motorbikes Emissions
     *
     * @param object $usages
     */
    public function calculateMotorbikesEmissions($usages)
    {
        return $usages->filter(function ($motorbike) {
            return $motorbike->motorbike_type != null && $motorbike->fuel_per_week != null;
        })->map(function ($motorbike) {
            $factor = MotorbikeConversionFactor::where('motorbike_id', $motorbike->motorbike_type)
                    ->where('measurement_type', $motorbike->preferred_measurement)
                    ->first();
            
            if (!empty($factor)) {
                $litres = ($motorbike->fuel_per_week)/config('reduce.calculations.petrol_per_litre');

                $emission =  config('reduce.calculations.equivalent_of_tonne') * config('reduce.calculations.weeks_in_a_month') *
                ($litres * $factor->conversion_factor);
            } else {
                $emission = 0;
            }

            $this->saveEmissionPerVehicle($motorbike, $emission);

            return $emission;
            // dd($emission);
        });
    }

    /**
     * Calculate Vehicles Emissions
     *
     * @param object $usages
     */
    public function calculateVehiclesEmissions($usages)
    {
       
        return $usages->filter(function ($vehicle) {
            return $vehicle->vehicle_type != null && $vehicle->fuel_per_week != null;
        })->map(function ($vehicle) {
            $factor = VehicleFootPrintFactor::where('vehicle_type', $vehicle->vehicle_type)
                    ->where('fuel_type', $vehicle->vehicle_fuel_type)
                    ->where('measurement_type', $vehicle->preferred_measurement)
                    ->first();
        //   dd($factor);

            if (!empty($factor)) {
                if ($vehicle->vehicle_fuel_type == 1) {
                    $litres = ($vehicle->fuel_per_week)/config('reduce.calculations.petrol_per_litre');
                } elseif ($vehicle->vehicle_fuel_type == 2) {
                     $litres = ($vehicle->fuel_per_week)/config('reduce.calculations.diesel_per_litre');
                }


                $emission =  config('reduce.calculations.equivalent_of_tonne') * config('reduce.calculations.weeks_in_a_month') *
                ($litres * $factor->conversion_factor);
                // dd($emission);
            } else {
                $emission = 0;
            }

            $this->saveEmissionPerVehicle($vehicle, $emission);

            return $emission;
        });
    }

    /**
     * Save Each Vehicles Emission
     *
     * @param object $vehicle
     * @param integer|string $emission
     */
    public function saveEmissionPerVehicle($vehicle, $emission)
    {
        $emissions = UserVehicleCarbonEmission::firstOrCreate([
            'user_id' => Auth::user()->id,
            'vehicle' => $vehicle->vehicle_name,
            'usage_type' => $vehicle->usage_type,
            'vehicle_type' => $vehicle->vehicle_type,
            'motorbike_type' => $vehicle->motorbike_type,
            'fuel_type' => $vehicle->vehicle_fuel_type,
            'usage_type_id' => $vehicle->usage_type_id
        ]);
        // dd($emissions);
        if ($emissions->save()) {
            // dd($emissions);

            $emissions->update([
                'emitted_amount' => $emission
            ]);
        }

        return $emissions;
    }

    /**
     * Save User Vehicle Total Emissions
     */
    public function saveUserVehiclesTotalEmissions($emission, $userId)
    {
        $totalEmissions = UserVehicleTotalEmission::firstOrCreate([
            'user_id' => $userId
        ]);
// dd($totalEmissions);
        if ($totalEmissions->save()) {
            // dd($totalEmissions);

            $totalEmissions->update([
                'total_emissions' => $emission
            ]);
        }

        return $totalEmissions;
    }

    /**
     * Get Vehicle Breakdown Calculation
     */
    public function vehicleEmissionsBreakDown($userId)
    {
        if (Auth()->id() != $userId) {
            return response()->json([
                'status' => 'error',
                'msg' => 'You have no access to this account kindly login and view your account'
            ], 403);
        }

        $userEmissions = UserVehicleCarbonEmission::where('user_id', $userId)
                    ->select(
                        'vehicle',
                        'fuel_type',
                        'emitted_amount'
                    )->get();

        $total = $userEmissions->sum('emitted_amount');

        $breakdown = $userEmissions->groupBy('vehicle')->map(function ($vehicles) use ($total) {
            $vehicle = $vehicles->unique()->pluck('vehicle');

            $sum = $vehicles->sum('emitted_amount');
// dd($total);
            $percentage = round(($sum/$total) * config('reduce.calculations.total_percentage'), config('reduce.calculations.round_of_to'));
            return [
                'vehicle_name' => $vehicle->implode(''),
                'emitted_amount' => round($sum, config('reduce.calculations.round_of_to')),
                'percentage' => $percentage
            ];
        })->values();

        return response()->json([
            'data' => $breakdown,
            'status' => 'success',
            'msg' => 'User vehicles breakdown retrieved successfully'
        ]);
    }
}
