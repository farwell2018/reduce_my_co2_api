<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use App\Models\UserVehicleCarbonEmission;
use App\Models\UserVehicleUsage;

class VehicleCommutesAndJourneysController extends Controller
{
    /**
     * Compare Commutes to Journeys
     *
     * @param integer userId
     */
    public function compareCommutesToJourneys($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $emissions = UserVehicleCarbonEmission::where('user_id', $userId)->get();

        $journeys = $emissions->filter(function ($emission) {
            return $emission->usage_type == UserVehicleUsage::JOURNEY_USAGE;
        })->sum('emitted_amount');

        $commutes = $emissions->filter(function ($emission) {
            return $emission->usage_type == UserVehicleUsage::COMMUTE_USAGE;
        })->sum('emitted_amount');

        $total = $journeys + $commutes;

        $journeyPercentage = round(($journeys/$total) * config('reduce.calculations.total_percentage'), config('reduce.calculations.round_of_to'));
        $commutePercentage = round(($commutes/$total) * config('reduce.calculations.total_percentage'), config('reduce.calculations.round_of_to'));

        $data = [
            'total' => round($total, config('reduce.calculations.round_of_to')) . 't',
            'journeys' => [
                'emissions' => round($journeys, config('reduce.calculations.round_of_to')) . 't',
                'percentage' => $journeyPercentage . '%'
            ],
            'commutes' => [
                'emissions' => round($commutes, config('reduce.calculations.round_of_to')) . 't',
                'percentage' => $commutePercentage . '%'
            ]
        ];

        return response()->json([
            'data' => $data,
            'status' => 'success',
            'msg' => 'Comparison retrieved successfully'
        ], 200);
    }
}
