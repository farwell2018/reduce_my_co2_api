<?php

namespace App\Http\Controllers\Calculations;

use App\Http\Controllers\Controller;
use App\Models\UserDietTotalEmission;
use App\Models\UserEmissionLog;
use App\Models\UserFlightsTotalEmission;
use App\Models\UserHouseHoldTotalEmission;
use App\Models\UserVehicleCarbonEmission;
use App\Models\UserVehicleTotalEmission;
use App\Models\MpesaTransaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mpesa;
use Carbon\Carbon;

class CalculateAllEmissionsController extends Controller
{


   
    
    /**
     * Calculate All Emissions
     *
     * @param integer userId
     */
    public function calculateAllCombinedEmissions($userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        }

        $vehicleEmissions = UserVehicleTotalEmission::where('user_id', $userId)->first();

        $flightEmissions = UserFlightsTotalEmission::where('user_id', $userId)->first();

        $houseHoldEmissions = UserHouseHoldTotalEmission::where('user_id', $userId)->first();

        $dietEmissions = UserDietTotalEmission::where('user_id', $userId)->first();

        $flight = isset($flightEmissions->total_emissions) ? $flightEmissions->total_emissions : 0;

        $vehicle = isset($vehicleEmissions->total_emissions) ? $vehicleEmissions->total_emissions : 0;

        $household = isset($houseHoldEmissions->total_emissions) ? $houseHoldEmissions->total_emissions : 0;

        $food = isset($dietEmissions->total_emissions) ? $dietEmissions->total_emissions : 0;

        $total = $vehicle + $flight + $household + $food;

        $data = [
            'total' => round($total, config('reduce.calculations.round_of_to')),

            'flight' => [
                'emission' => $flight . 't',
                'percentage' => round(($flight / $total) * UserVehicleCarbonEmission::TOTAL_PERCENTAGE, config('reduce.calculations.round_of_to'))
            ],
            'household' => [
                'emission' => $household . 't',
                'percentage' => round(($household / $total) * UserVehicleCarbonEmission::TOTAL_PERCENTAGE, config('reduce.calculations.round_of_to'))
            ],
            'food' => [
                'emission' => $food . 't',
                'percentage' => round(($food / $total) * UserVehicleCarbonEmission::TOTAL_PERCENTAGE, config('reduce.calculations.round_of_to'))
            ],
            'vehicle' => [
                'emission' => $vehicle . 't',
                'percentage' => round(($vehicle / $total) * UserVehicleCarbonEmission::TOTAL_PERCENTAGE, config('reduce.calculations.round_of_to'))
            ],
            'date_last_calculated' => $this->getDateLastCalculated($userId)->toFormattedDateString()
        ];

        return response()->json([
            'status' => 'success',
            'data' => $data,
            'msg' => 'Total emissions retrieved successfully'
        ], 200);
    }

    /**
     * Get date last calculated
     */
    public function getDateLastCalculated($id)
    {
        $logs = UserEmissionLog::where('user_id', $id)->latest()->first();

        return $logs->created_at;
    }

    public function makePaymentCombinedEmissions(Request $request, $userId)
    {
        if (Auth()->id() != $userId) {
            return $this->errorResponse('You do not have access to this account!', 403);
        } else {
            $user = User::getSingleUser($userId);

            
        }

        $vehicleEmissions = UserVehicleTotalEmission::where('user_id', $userId)->first();

        $flightEmissions = UserFlightsTotalEmission::where('user_id', $userId)->first();

        $houseHoldEmissions = UserHouseHoldTotalEmission::where('user_id', $userId)->first();

        $dietEmissions = UserDietTotalEmission::where('user_id', $userId)->first();

        $flight = isset($flightEmissions->total_emissions) ? $flightEmissions->total_emissions : 0;

        $vehicle = isset($vehicleEmissions->total_emissions) ? $vehicleEmissions->total_emissions : 0;

        $household = isset($houseHoldEmissions->total_emissions) ? $houseHoldEmissions->total_emissions : 0;

        $food = isset($dietEmissions->total_emissions) ? $dietEmissions->total_emissions : 0;

        $total = $vehicle + $flight + $household + $food;

        $actual_total = round($total, config('reduce.calculations.round_of_to'));

        $offset =  (($actual_total / 0.5) * 0.5);

        $amount = round(($offset * 14), config('reduce.calculations.round_of_to'));


        $selection = $request->input('selection');
        $number = $request->input('number');

        if ($selection === 'registration_number') {

            $pnumber = $user->phone_number;
        } else {
            $pnumber = $number;
        }

            $amount = 1;
            
            if(substr($pnumber, 0, 1) === '+'){
                $phoneNumber = str_replace('+','',$pnumber);
    
              }else if(substr($pnumber, 0, 1) === '0'){
    
                $phoneNumber = str_replace('0','254',$pnumber);
              }else{
    
                $phoneNumber = $pnumber;
              }
    
            $url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
            $curl_post_data = [
                'BusinessShortCode' =>174379,
                'Password' => $this->lipaNaMpesaPassword(),
                'Timestamp' => Carbon::rawParse('now')->format('YmdHms'),
                'TransactionType' => 'CustomerPayBillOnline',
                'Amount' => $amount,
                'PartyA' => $phoneNumber, 
                'PartyB' => 174379,
                'PhoneNumber' => $phoneNumber, 
                'CallBackURL' => 'https://reducemyco2.farwell-consultants.com/api/stk/push/callback/url',
                'AccountReference' => "ReduceMyCO2 ",
                'TransactionDesc' => "Donation"
            ];
    
    
            $data_string = json_encode($curl_post_data);
    
    
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->newAccessToken()));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            $curl_response = curl_exec($curl);
    
            return $curl_response; 
    
        // $x = json_decode($payment);
            
        //  if($x->ResponseCode == 0){

        
         //}
       
        // $mresponse = $this->mpesaRes();
       
    }

    public function stkPush($amount, $pnumber)
    {  
      
       
        
        
     } 


  public function mpesaRes(Request $request){

    $response = json_decode($request->getContent());

    $trn = new MpesaTransaction;
    $trn->response = json_encode($response);
    $trn->save();

     return true;
  }



  public function lipaNaMpesaPassword()
  {
      //timestamp
      $timestamp = Carbon::rawParse('now')->format('YmdHms');
      //passkey
      $passKey ="bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919";
      $businessShortCOde =174379;
      //generate password
      $mpesaPassword = base64_encode($businessShortCOde.$passKey.$timestamp);

      return $mpesaPassword;
      
  }
  

  public function newAccessToken()
  {
      $consumer_key= env('MPESA_CONSUMER_KEY');
      $consumer_secret= env('MPESA_CONSUMER_SECRET');
      $credentials = base64_encode($consumer_key.":".$consumer_secret);
      $url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials";


      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Basic ".$credentials,"Content-Type:application/json"));
      curl_setopt($curl, CURLOPT_HEADER,false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $curl_response = curl_exec($curl);
      $access_token=json_decode($curl_response);
      curl_close($curl);
     
      return $access_token->access_token;  
      
  }


}
