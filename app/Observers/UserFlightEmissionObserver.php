<?php

namespace App\Observers;

use App\Models\UserEmissionLog;
use App\Models\UserFlightsTotalEmission;

class UserFlightEmissionObserver
{
    /**
     * Handle the UserFlightsTotalEmission "created" event.
     *
     * @param  \App\Models\UserFlightsTotalEmission  $userFlightsTotalEmission
     * @return void
     */
    public function created(UserFlightsTotalEmission $userFlightsTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::FLIGHT_EMISSIONS,
            'emission_id' => $userFlightsTotalEmission->id,
            'user_id' => $userFlightsTotalEmission->user_id,
            'total_emission' => $userFlightsTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserFlightsTotalEmission "updated" event.
     *
     * @param  \App\Models\UserFlightsTotalEmission  $userFlightsTotalEmission
     * @return void
     */
    public function updated(UserFlightsTotalEmission $userFlightsTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::FLIGHT_EMISSIONS,
            'emission_id' => $userFlightsTotalEmission->id,
            'user_id' => $userFlightsTotalEmission->user_id,
            'total_emission' => $userFlightsTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserFlightsTotalEmission "deleted" event.
     *
     * @param  \App\Models\UserFlightsTotalEmission  $userFlightsTotalEmission
     * @return void
     */
    public function deleted(UserFlightsTotalEmission $userFlightsTotalEmission)
    {
        //
    }

    /**
     * Handle the UserFlightsTotalEmission "restored" event.
     *
     * @param  \App\Models\UserFlightsTotalEmission  $userFlightsTotalEmission
     * @return void
     */
    public function restored(UserFlightsTotalEmission $userFlightsTotalEmission)
    {
        //
    }

    /**
     * Handle the UserFlightsTotalEmission "force deleted" event.
     *
     * @param  \App\Models\UserFlightsTotalEmission  $userFlightsTotalEmission
     * @return void
     */
    public function forceDeleted(UserFlightsTotalEmission $userFlightsTotalEmission)
    {
        //
    }
}
