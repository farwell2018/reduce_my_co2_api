<?php

namespace App\Observers;

use App\Models\UserDietTotalEmission;
use App\Models\UserEmissionLog;

class UserDietEmissionObserver
{
    /**
     * Handle the UserDietTotalEmission "created" event.
     *
     * @param  \App\Models\UserDietTotalEmission  $userDietTotalEmission
     * @return void
     */
    public function created(UserDietTotalEmission $userDietTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::FOOD_EMISSIONS,
            'emission_id' => $userDietTotalEmission->id,
            'user_id' => $userDietTotalEmission->user_id,
            'total_emission' => $userDietTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserDietTotalEmission "updated" event.
     *
     * @param  \App\Models\UserDietTotalEmission  $userDietTotalEmission
     * @return void
     */
    public function updated(UserDietTotalEmission $userDietTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::FOOD_EMISSIONS,
            'emission_id' => $userDietTotalEmission->id,
            'user_id' => $userDietTotalEmission->user_id,
            'total_emission' => $userDietTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserDietTotalEmission "deleted" event.
     *
     * @param  \App\Models\UserDietTotalEmission  $userDietTotalEmission
     * @return void
     */
    public function deleted(UserDietTotalEmission $userDietTotalEmission)
    {
        //
    }

    /**
     * Handle the UserDietTotalEmission "restored" event.
     *
     * @param  \App\Models\UserDietTotalEmission  $userDietTotalEmission
     * @return void
     */
    public function restored(UserDietTotalEmission $userDietTotalEmission)
    {
        //
    }

    /**
     * Handle the UserDietTotalEmission "force deleted" event.
     *
     * @param  \App\Models\UserDietTotalEmission  $userDietTotalEmission
     * @return void
     */
    public function forceDeleted(UserDietTotalEmission $userDietTotalEmission)
    {
        //
    }
}
