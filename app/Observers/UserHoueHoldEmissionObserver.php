<?php

namespace App\Observers;

use App\Models\UserEmissionLog;
use App\Models\UserHouseHoldTotalEmission;

class UserHoueHoldEmissionObserver
{
    /**
     * Handle the UserHouseHoldTotalEmission "created" event.
     *
     * @param  \App\Models\UserHouseHoldTotalEmission  $userHouseHoldTotalEmission
     * @return void
     */
    public function created(UserHouseHoldTotalEmission $userHouseHoldTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::HOUSEHOLD_EMISSIONS,
            'emission_id' => $userHouseHoldTotalEmission->id,
            'user_id' => $userHouseHoldTotalEmission->user_id,
            'total_emission' => $userHouseHoldTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserHouseHoldTotalEmission "updated" event.
     *
     * @param  \App\Models\UserHouseHoldTotalEmission  $userHouseHoldTotalEmission
     * @return void
     */
    public function updated(UserHouseHoldTotalEmission $userHouseHoldTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::HOUSEHOLD_EMISSIONS,
            'emission_id' => $userHouseHoldTotalEmission->id,
            'user_id' => $userHouseHoldTotalEmission->user_id,
            'total_emission' => $userHouseHoldTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserHouseHoldTotalEmission "deleted" event.
     *
     * @param  \App\Models\UserHouseHoldTotalEmission  $userHouseHoldTotalEmission
     * @return void
     */
    public function deleted(UserHouseHoldTotalEmission $userHouseHoldTotalEmission)
    {
        //
    }

    /**
     * Handle the UserHouseHoldTotalEmission "restored" event.
     *
     * @param  \App\Models\UserHouseHoldTotalEmission  $userHouseHoldTotalEmission
     * @return void
     */
    public function restored(UserHouseHoldTotalEmission $userHouseHoldTotalEmission)
    {
        //
    }

    /**
     * Handle the UserHouseHoldTotalEmission "force deleted" event.
     *
     * @param  \App\Models\UserHouseHoldTotalEmission  $userHouseHoldTotalEmission
     * @return void
     */
    public function forceDeleted(UserHouseHoldTotalEmission $userHouseHoldTotalEmission)
    {
        //
    }
}
