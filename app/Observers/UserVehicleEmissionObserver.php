<?php

namespace App\Observers;

use App\Models\UserEmissionLog;
use App\Models\UserVehicleTotalEmission;

class UserVehicleEmissionObserver
{
    /**
     * Handle the UserVehicleTotalEmission "created" event.
     *
     * @param  \App\Models\UserVehicleTotalEmission  $userVehicleTotalEmission
     * @return void
     */
    public function created(UserVehicleTotalEmission $userVehicleTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::VEHICLE_EMISSIONS,
            'emission_id' => $userVehicleTotalEmission->id,
            'user_id' => $userVehicleTotalEmission->user_id,
            'total_emission' => $userVehicleTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserVehicleTotalEmission "updated" event.
     *
     * @param  \App\Models\UserVehicleTotalEmission  $userVehicleTotalEmission
     * @return void
     */
    public function updated(UserVehicleTotalEmission $userVehicleTotalEmission)
    {
        UserEmissionLog::create([
            'emission_type' => UserEmissionLog::VEHICLE_EMISSIONS,
            'emission_id' => $userVehicleTotalEmission->id,
            'user_id' => $userVehicleTotalEmission->user_id,
            'total_emission' => $userVehicleTotalEmission->total_emissions
        ]);
    }

    /**
     * Handle the UserVehicleTotalEmission "deleted" event.
     *
     * @param  \App\Models\UserVehicleTotalEmission  $userVehicleTotalEmission
     * @return void
     */
    public function deleted(UserVehicleTotalEmission $userVehicleTotalEmission)
    {
        //
    }

    /**
     * Handle the UserVehicleTotalEmission "restored" event.
     *
     * @param  \App\Models\UserVehicleTotalEmission  $userVehicleTotalEmission
     * @return void
     */
    public function restored(UserVehicleTotalEmission $userVehicleTotalEmission)
    {
        //
    }

    /**
     * Handle the UserVehicleTotalEmission "force deleted" event.
     *
     * @param  \App\Models\UserVehicleTotalEmission  $userVehicleTotalEmission
     * @return void
     */
    public function forceDeleted(UserVehicleTotalEmission $userVehicleTotalEmission)
    {
        //
    }
}
