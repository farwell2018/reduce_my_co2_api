<?php

namespace App\Providers;

use App\Models\UserDietTotalEmission;
use App\Models\UserFlightsTotalEmission;
use App\Models\UserHouseHoldTotalEmission;
use App\Models\UserVehicleTotalEmission;
use App\Observers\UserDietEmissionObserver;
use App\Observers\UserFlightEmissionObserver;
use App\Observers\UserHoueHoldEmissionObserver;
use App\Observers\UserVehicleEmissionObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        UserVehicleTotalEmission::observe(UserVehicleEmissionObserver::class);
        UserFlightsTotalEmission::observe(UserFlightEmissionObserver::class);
        UserDietTotalEmission::observe(UserDietEmissionObserver::class);
        UserHouseHoldTotalEmission::observe(UserHoueHoldEmissionObserver::class);
    }
}
