<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleFootPrintFactor extends Model
{
    use HasFactory;

    public const WEEKS_IN_A_YEAR = 52;

    public const WEEKS_IN_A_MONTH = 4;

    public const EQUIVALENT_OF_ONE_TONNE = 0.001;

    protected $fillable = [
        'vehicle_type', 'fuel_type'
    ];
}
