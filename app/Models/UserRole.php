<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use HasFactory;

    protected $fillable = [];

    /**
     * Relationship with user
     *
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
