<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserHomeDetail extends Model
{
    use HasFactory;

    const HOUSE = 0;
    const APARTMENT = 1;
    const STUDENT_DORM = 2;

    const VERYSMALL = 0;
    const SMALL = 1;
    const MEDIUM = 2;
    const LARGE = 3;
    const HUGE = 4;

    const YES = 1;
    const NO = 0;

    protected $fillable = [
        'user_id', 'lives_in', 'home_size', 'no_of_shared_people', 'knows_electricity_consumption',
        'heats_house_with', 'heat_consumption_amount', 'heats_water_with', 'knows_water_consumption',
        'water_consumption_amount', 'electricity_consumption'
    ];

    /**
     * House
     */
    public static function getHomes()
    {
        $homes =[
            ['id' => self::HOUSE, 'type' => config('reduce.homes.house')],
            ['id' => self::APARTMENT, 'type' => config('reduce.homes.apartment')],
            ['id' => self::STUDENT_DORM, 'type' => config('reduce.homes.student_dorm')],
        ];

        return $homes;
    }

    /**
     * Get House Sizes
     */
    public static function getHouseSizes()
    {
        $sizes =[
            ['id' => self::VERYSMALL, 'type' => config('reduce.house_size.very_small')],
            ['id' => self::SMALL, 'type' => config('reduce.house_size.small')],
            ['id' => self::MEDIUM, 'type' => config('reduce.house_size.medium')],
            ['id' => self::LARGE, 'type' => config('reduce.house_size.large')],
            ['id' => self::HUGE, 'type' => config('reduce.house_size.huge')],
        ];

        return $sizes;
    }

    /**
     * Get a yes on no
     */
    public static function getYesOrNo()
    {
        $answer =[
            ['id' => self::YES, 'type' => config('reduce.yes_or_no.yes')],
            ['id' => self::NO, 'type' => config('reduce.yes_or_no.no')]
        ];

        return $answer;
    }

    public static function houseHeats()
    {
        $heats = [
            ['id' => self::VERYSMALL, 'type' => config('reduce.house_heats.electricity')],
            ['id' => self::SMALL, 'type' => config('reduce.house_heats.solar')],
            ['id' => self::MEDIUM, 'type' => config('reduce.house_heats.gas')],
            ['id' => self::LARGE, 'type' => config('reduce.house_heats.heating_oil')],
            ['id' => self::HUGE, 'type' => config('reduce.house_heats.wood')],
        ];

        return $heats;
    }
}
