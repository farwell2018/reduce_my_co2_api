<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Newsfeed extends Model
{
    use HasFactory;

    const ACTIVE_FEED = 1;
    const INACTIVE_FEED = 0;

    protected $fillable = [
        'title', 'url', 'status', 'user_id'
    ];

    /**
     * Relationship with user
     *
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
