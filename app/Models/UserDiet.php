<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Guid\Guid;

class UserDiet extends Model
{
    use HasFactory;

    public const OMNIVORE = 0;
    public const CANIVORE = 1;
    public const PESCATARIAN = 2;
    public const VEGETARIAN = 3;
    public const VEGAN = 4;

    protected $fillable = [
        'user_id', 'user_diet_preference', 'knows_calories_i_consume',
        'calories', 'weight', 'height' , 'people' , 'beef' , 'lamb' , 'prawns' , 'fish' , 'chicken' , 'cheese' , 'milk' , 'eggs' , 'beans' , 'pork'
    ];

    /**
     * Get all Preferences
     */
    public static function dietPreferences()
    {
        $diet_preference =[
            ['id' => self::OMNIVORE, 'type' => config('reduce.my_diet_preference.omnivore')],
            ['id' => self::CANIVORE, 'type' => config('reduce.my_diet_preference.canivore')],
            ['id' => self::PESCATARIAN, 'type' => config('reduce.my_diet_preference.pescaterian')],
            ['id' => self::VEGETARIAN, 'type' => config('reduce.my_diet_preference.vegetarian')],
            ['id' => self::VEGAN, 'type' => config('reduce.my_diet_preference.vegan')],
        ];

        return $diet_preference;
    }

    public static function estimateCalories(User $user, $weight, $height)
    {

    }
}
