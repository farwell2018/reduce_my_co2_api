<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    const ACTIVE_COUNTRY = 1;
    const INACTIVE_COUNTRY = 0;

    protected $fillable = [
        'name', 'code', 'tel_code', 'status', 'flag', 'demonym'
    ];

    /**
     * Relationship with user
     *
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
