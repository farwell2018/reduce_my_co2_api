<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlightUsage extends Model
{
    use HasFactory;

    const ONE_WAY = 0;
    const ROUND = 1;

    protected $fillable = [
        'user_id', 'flight_type', 'flight_class',
        'distance', 'one_way_or_round',
        'number_of_passengers', 'destination', 'purpose', 'model', 'engine_type', 'duration'
    ];
}
