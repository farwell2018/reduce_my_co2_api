<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserNewsFeed extends Model
{
    use HasFactory;

    protected $table ='user_newsfeed';

    protected $fillable = [
        'user_id', 'newsfeed'
    ];

    /**
     * Many Vehicles can be owned by one user
     *
     *@return \Illuminate\Database\Eloquent\Relations
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
