<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVehicleEcoProfile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'vehicle_name', 'vehicle_type',
        'vehicle_fuel_type', 'motorbike_type', 'vehicle_fuel_type', 'fuel_per_week'
    ];

    /**
     * Many Vehicles can be owned by one user
     *
     *@return \Illuminate\Database\Eloquent\Relations
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
