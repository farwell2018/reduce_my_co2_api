<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Motorbike extends Model
{
    use HasFactory;

    const ACTIVE_MOTORBIKE = 1;
    const INACTIVE_MOTORBIKE = 0;
}
