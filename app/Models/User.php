<?php

namespace App\Models;

use App\Jobs\EmailVerificationJob;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use HasFactory;
    use Notifiable;
    /**
     * User constants
     */
    public const IS_ADMIN = 1;
    public const IS_REGULAR_USER = 2;
    public const IS_VERIFIED = 1;
    public const UNVERIFIED_USER = 0;

    /**
     * Profile Constants
     */
    public const PROFILE_IS_COMPLETE = 1;
    public const PROFILE_NOT_COMPLETE = 0;
    public const OWNS_VEHICLE = 1;
    public const DOES_NOT_OWN_A_VEHICLE = 0;

    /**
     * Measurement types(Metric)
     *
     * Weight-KG, Distance-KM, Fluid-LITRES
     */
    public const METRIC = 0;

    /**
     * Measurement types(Imperial)
     *
     * Weight-Lb, Distance-Miles, Fluid-Gallons
     */
    public const IMPERIAL = 1;

    /**
     * Gender
     *
     * @return void
     */
    public const HIDE_GENDER = 0;
    public const FEMALE_GENDER = 1;
    public const MALE_GENDER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','email','password', 'first_name', 'last_name',
        'other_name', 'phone_number', 'username', 'gender',
        'country_id', 'role_id', 'year_of_birth', 'twitter_handle',
        'verification_code', 'verified_at', 'is_verified', 'preferred_measurement',
        'avatar', 'profile_is_complete', 'owns_vehicle'
    ];

    /**
     * Appending attributes that are not in the database as fields but defined through as Accessors
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'user_preferred_measurement',
        'gender_type'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        // 'verification_code'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Generate user verification code
     *
     * @return string
     */
    public static function generateVerificationCode()
    {
        return random_int(100000, 999999);
    }

    /**
     * Set First Name Attribute
     *
     * @return string
     */
    public function setFirstNameAttribute($first_name)
    {
        return $this->attributes['first_name'] = $first_name;
    }

    /**
     * Get First Name Attribute
     *
     * @return string
     */
    public function getFirstNameAttribute($first_name)
    {
        return ucwords($first_name);
    }

    /**
     * Set Last Name Attribute
     *
     * @return string
     */
    public function setLastNameAttribute($last_name)
    {
        return $this->attributes['last_name'] = $last_name;
    }

    /**
     * Get Last Name Attribute
     *
     * @return string
     */
    public function getLastNameAttribute($last_name)
    {
        return ucwords($last_name);
    }

    /**
     * Set Other Name Attribute
     *
     * @return string
     */
    public function setOtherNameAttribute($other_name)
    {
        return $this->attributes['other_name'] = $other_name;
    }

    /**
     * Get Other Name Attribute
     *
     * @return string
     */
    public function getOtherNameAttribute($other_name)
    {
        return ucwords($other_name);
    }

    /**
     * Set Email Attribute
     *
     * @return email
     */
    public function setEmailAttribute(string $email)
    {
        return $this->attributes['email'] = strtolower($email);
    }

    /**
     * Get Full Name Attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        if ($this->other_name) {
            return "{$this->first_name} {$this->other_name} {$this->last_name}";
        } else {
            return "{$this->first_name} {$this->last_name}";
        }
    }

    /**
     * Get an Admin User
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->role_id == User::IS_ADMIN;
    }

    /**
     * Get verified user
     *
     * @return boolean
     */
    public function isVerified()
    {
        return $this->is_verified == User::IS_VERIFIED;
    }

    /**
     * Get gender
     *
     * @return array
     */
    public static function getGender()
    {
        $gender =[
            ['id' => User::HIDE_GENDER, 'type' => 'Prefer not to say'],
            ['id' => User::FEMALE_GENDER, 'type' => 'Female'],
            ['id' => User::MALE_GENDER, 'type' => 'Male'],
        ];

        return $gender;
    }

    /**
     * Get Preferred Measurement type
     *
     * @return array
     */
    public static function getMeasurementMethod()
    {
        $methods = [
            ['id' => User::METRIC, 'name' => 'Metric'],
            ['id' => User::IMPERIAL, 'name' => 'Imperial']
        ];

        return $methods;
    }

    /**
     * Append measurement type to user
     * @return string
     */
    public function getUserPreferredMeasurementAttribute()
    {
        if ($this->preferred_measurement == self::METRIC) {
            return self::getMeasurementMethod()[0]['name'];
        } else {
            return self::getMeasurementMethod()[1]['name'];
        }
    }

    /**
     * Append user gender type
     * @return string
     */
    public function getGenderTypeAttribute()
    {
        if ($this->gender == self::HIDE_GENDER) {
            return self::getGender()[0]['type'];
        } elseif ($this->gender == self::FEMALE_GENDER) {
            return self::getGender()[1]['type'];
        } else {
            return self::getGender()[2]['type'];
        }
    }

    /**
     * Create an Admin user
     *
     * @param array
     * @return array
     */
    public function createAdminUser($details = [])
    {
        $user = new self($details);

        if ($this->checkIfAdminExists() <= 0) {
            $user->role_id = self::IS_ADMIN;
        }

        $user->verification_code = self::generateVerificationCode();

        $data = [
            'name' => $user->full_name,
            'email' => $user->email,
            'code' => $user->verification_code
        ];

        EmailVerificationJob::dispatch($data);

        $user->save();

        return $user;
    }

    /**
     * Check if admin already exists
     *
     * @return integer
     */
    public function checkIfAdminExists()
    {
        return self::where('role_id', 1)->count();
    }

    /**
     * Relationship with country
     *
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    /**
     * Relationship with roles
     *
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function role()
    {
        return $this->hasOne(UserRole::class);
    }

    /**
     * A single user can own many vehicles
     *
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function vehicles()
    {
        return $this->hasMany(UserVehicleEcoProfile::class);
    }

        /**
     * A single user can have many feeds
     *
     * @return \Illuminate\Database\Eloquent\Relations
     */
    public function userNewsFeed()
    {
        return $this->hasMany(UserNewsFeed::class);
    }


    public function news()
    {

        return $this->belongsTo(Newsfeed::class);
    }
    /**
     * Get Single user
     *
     * @return Collection
     */
    public static function getSingleUser($id)
    {
        return User::where('id', $id)->first();
    }

    /**
     * Calculate users age
     */
    public function calculateAge()
    {
        if($this->year_of_birth) {
            return Carbon::now()->year - $this->year_of_birth;
        }
    }
}
