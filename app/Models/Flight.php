<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    use HasFactory;

    const ACTIVE_FLIGHT_TYPE = 1;
    const INACTIVE_FLIGHT_TYPE = 0;

    const EQUIVALENT_OF_ONE_TONNE = 0.001;
    const ROUND_OF_TO = 2;

    const MONTHS_IN_A_YEAR = 12;

    protected $fillable = ['flight_type'];
}
