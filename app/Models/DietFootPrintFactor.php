<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DietFootPrintFactor extends Model
{
    use HasFactory;

    protected $fillable = [
        'food_type', 'total_emissions'
    
    ];
}
