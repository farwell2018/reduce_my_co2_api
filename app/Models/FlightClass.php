<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlightClass extends Model
{
    use HasFactory;

    const ACTIVE_FLIGHT_CLASS = 1;
    const INACTIVE_FLIGHT_CLASS = 0;
}
