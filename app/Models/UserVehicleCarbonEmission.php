<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVehicleCarbonEmission extends Model
{
    use HasFactory;

    public const TOTAL_PERCENTAGE = 100;
    public const ROUND_OF_TO = 2;

    protected $fillable = [
        'user_id', 'vehicle', 'vehicle_type', 'fuel_type',
        'emitted_amount', 'usage_type', 'motorbike_type',
        'usage_type_id'
    ];
}
