<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserVehicleUsage extends Model
{
    use HasFactory;

    const COMMUTE_USAGE = 'commute';
    const JOURNEY_USAGE = 'journey';

    protected $fillable = [
        'user_id', 'vehicle_name', 'usage_type', 'usage_type_id', 'user_vehicle_eco_profile_id',
        'days_per_week', 'distance_per_week', 'distance_per_day'
    ];
}
