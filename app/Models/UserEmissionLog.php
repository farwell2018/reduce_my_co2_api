<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserEmissionLog extends Model
{
    use HasFactory;

    public const VEHICLE_EMISSIONS = 'vehicle';
    public const FLIGHT_EMISSIONS = 'flight';
    public const FOOD_EMISSIONS = 'food';
    public const HOUSEHOLD_EMISSIONS = 'household';

    protected $fillable = [
        'emission_type', 'emission_id', 'user_id', 'total_emission'
    ];
}
