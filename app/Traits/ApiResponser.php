<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * ApiResponser
 */
trait ApiResponser
{
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json(['status' => 'error', 'msg' => $message], $code);
    }

    public function showOne(Model $model, $code = 200)
    {
        return $this->successResponse($model, $code);
    }
}
