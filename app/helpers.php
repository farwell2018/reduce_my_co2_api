<?php

use App\Models\User;

if (!function_exists('estimateCalories')) {
    function estimateCalories(User $user, $weight, $height)
    {
        // For men: BMR = 10 x weight (kg) + 6.25 x height (cm) – 5 x age (years) + 5
        // For women: BMR = 10 x weight (kg) + 6.25 x height (cm) – 5 x age (years) – 161
        if ($user->gender == User::FEMALE_GENDER) {
            return (10 * $weight) + (6.25 * $height) - (5 * $user->calculateAge()) - 161;
        }

        if ($user->gender == User::MALE_GENDER) {
            return (10 * $weight) + (6.25 * $height) - (5 * $user->calculateAge()) + 5;
        }
    }
}

if (!function_exists('estimatePowerUsage')) {
    function estimatePowerUsage($numberOfSharingPeople)
    {
        return 12.2 * $numberOfSharingPeople;
    }
}
