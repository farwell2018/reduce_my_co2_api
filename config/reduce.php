<?php

return [

    'homes' => [
        'house' => 'House',
        'apartment' => 'Apartment',
        'student_dorm' => 'Student Dorm'
    ],
    'house_size' => [
        'very_small' => 'Very Small - 1 Bedroom',
        'small' => 'Small - 2 Bedrooms',
        'medium' => 'Medium - 3 Bedrooms',
        'large' =>'Large - 4 Bedrooms',
        'huge' => 'Huge - 5+ Bedrooms'
    ],
    'yes_or_no' => [
        'yes' => 'Yes',
        'no' => 'No'
    ],
    'house_heats' => [
        'electricity' => 'Electricity',
        'solar' => 'Solar',
        'gas' => 'Gas',
        'heating_oil' => 'Heating Oil',
        'wood' => 'Wood'
    ],
    'calculations' => [
        'round_of_to' => 2,
        'total_percentage' => 100,
        'equivalent_of_tonne' => 0.001,
        'weeks_in_a_month' => 4,
        'petrol_per_litre' => 144.6,
        'diesel_per_litre' => 125.5
    ],
    'my_diet_preference' => [
        'omnivore' => 'Omnivore',
        'canivore' => 'Canivore',
        'pescaterian' => 'Pescatarian',
        'vegetarian' => 'Vegetarian',
        'vegan' => 'Vegan'
    ],
    'calories' => [
        'average_calories' => 2500,
        'average_emission' => 0.20833333
    ]
];
