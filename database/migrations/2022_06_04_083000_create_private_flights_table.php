<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrivateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('private_flights', function (Blueprint $table) {
            $table->id();
            $table->string('manufacturer');
            $table->string('aircraft');
            $table->integer('fuel_per_hour');
            $table->integer('capacity_seating');
            $table->integer('emissions_per_passenger_hour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('private_flights');
    }
}
