<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVehicleCarbonEmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_vehicle_carbon_emissions', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('vehicle');
            $table->unsignedBigInteger('vehicle_type')->nullable();
            $table->integer('motorbike_type')->nullable();
            $table->string('usage_type')->nullable();
            $table->unsignedBigInteger('fuel_type');
            $table->string('emitted_amount')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('vehicle_type')->references('id')->on('vehicles');
            $table->foreign('fuel_type')->references('id')->on('fuels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_vehicle_carbon_emissions');
    }
}
