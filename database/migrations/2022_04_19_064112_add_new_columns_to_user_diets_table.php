<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToUserDietsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_diets', function (Blueprint $table) {
                 $table->integer('beef')->after('user_diet_preference')->nullable();
            $table->integer('lamb')->nullable();
            $table->integer('prawns')->nullable();
            $table->integer('fish')->nullable();
            $table->integer('pork')->nullable();
            $table->integer('chicken')->nullable();
            $table->integer('cheese')->nullable();
            $table->integer('milk')->nullable();
            $table->integer('eggs')->nullable();
            $table->integer('beans')->nullable();
            $table->integer('people');
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_diets', function (Blueprint $table) {
            //
        });
    }
}
