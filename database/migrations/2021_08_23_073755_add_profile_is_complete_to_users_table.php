<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileIsCompleteToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
                if (!Schema::hasColumn('users', 'profile_is_complete')) {
                    $table->integer('profile_is_complete')->after('is_verified')->default(User::PROFILE_NOT_COMPLETE);
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
                if (Schema::hasColumn('users', 'profile_is_complete')) {
                    $table->dropColumn('profile_is_complete');
                }
            });
        }
    }
}
