<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPreferredMesurementToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
                if (!Schema::hasColumn('users', 'preferred_measurement')) {
                    $table->integer('preferred_measurement')->after('remember_token')->default(User::METRIC);
                }
                if (!Schema::hasColumn('users', 'avatar')) {
                    $table->string('avatar')->after('remember_token')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
                if (Schema::hasColumn('users', 'preferred_measurement')) {
                    $table->dropColumn('preferred_measurement');
                }
                if (Schema::hasColumn('users', 'avatar')) {
                    $table->dropColumn('avatar');
                }
            });
        }
    }
}
