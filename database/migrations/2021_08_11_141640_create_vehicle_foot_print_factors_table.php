<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleFootPrintFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_foot_print_factors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vehicle_type');
            $table->unsignedBigInteger('fuel_type');
            $table->integer('measurement_type');
            $table->float('conversion_factor', 8, 8);
            $table->timestamps();

            $table->foreign('vehicle_type')->references('id')->on('vehicles');
            $table->foreign('fuel_type')->references('id')->on('fuels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_foot_print_factors');
    }
}
