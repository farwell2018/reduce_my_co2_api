<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDistancePerDayToUserVehicleUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user_vehicle_usages')) {
            Schema::table('user_vehicle_usages', function (Blueprint $table) {
                if (!Schema::hasColumn('user_vehicle_usages', 'distance_per_day')) {
                    $table->integer('distance_per_day')->after('days_per_week')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_vehicle_usages')) {
            Schema::table('user_vehicle_usages', function (Blueprint $table) {
                if (Schema::hasColumn('user_vehicle_usages', 'distance_per_day')) {
                    $table->dropColumn('distance_per_day');
                }
            });
        }
    }
}
