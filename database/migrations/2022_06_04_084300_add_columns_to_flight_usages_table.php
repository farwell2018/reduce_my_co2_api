<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToFlightUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('flight_usages', function (Blueprint $table) {
            $table->decimal('duration', 5, 2)->after('distance');
            $table->string('engine_type')->after('duration');
            $table->string('model')->after('engine_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('flight_usages', function (Blueprint $table) {
            //
        });
    }
}
