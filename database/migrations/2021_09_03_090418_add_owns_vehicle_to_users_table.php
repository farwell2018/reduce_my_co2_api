<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOwnsVehicleToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
                if (!Schema::hasColumn('users', 'owns_vehicle')) {
                    $table->integer('owns_vehicle')->after('profile_is_complete')->default(User::DOES_NOT_OWN_A_VEHICLE);
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')){
            Schema::table('users', function (Blueprint $table) {
                if (Schema::hasColumn('users', 'owns_vehicle')) {
                    $table->dropColumn('owns_vehicle');
                }
            });
        }
    }
}
