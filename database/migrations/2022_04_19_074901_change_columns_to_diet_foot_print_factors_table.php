<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsToDietFootPrintFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diet_foot_print_factors', function (Blueprint $table) {
         
            $table->integer('user_type')->nullable()->change();
            $table->float('conversion_factor', 8, 8)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diet_foot_print_factors', function (Blueprint $table) {
            //
        });
    }
}
