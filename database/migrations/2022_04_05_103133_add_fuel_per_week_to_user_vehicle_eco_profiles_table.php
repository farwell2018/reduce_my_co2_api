<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFuelPerWeekToUserVehicleEcoProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_vehicle_eco_profiles', function (Blueprint $table) {
            $table->integer('fuel_per_week')->after('vehicle_fuel_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_vehicle_eco_profiles', function (Blueprint $table) {
            //
        });
    }
}
