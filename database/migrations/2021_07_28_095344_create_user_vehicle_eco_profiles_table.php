<?php

use App\Models\UserVehicleEcoProfile;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVehicleEcoProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_vehicle_eco_profiles')) {
            Schema::create('user_vehicle_eco_profiles', function (Blueprint $table) {
                $table->id();
                $table->integer('user_id');
                $table->string('vehicle_name');
                $table->unsignedBigInteger('vehicle_type')->nullable();
                $table->unsignedBigInteger('vehicle_fuel_type')->nullable();
                $table->integer('motorbike_type')->nullable();
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('vehicle_type')->references('id')->on('vehicles');
                $table->foreign('vehicle_fuel_type')->references('id')->on('fuels');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('user_vehicle_eco_profiles')){
            Schema::dropIfExists('user_vehicle_eco_profiles');
        }
    }
}
