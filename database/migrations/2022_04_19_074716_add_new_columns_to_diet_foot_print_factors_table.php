<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToDietFootPrintFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diet_foot_print_factors', function (Blueprint $table) {
            $table->string('food_type')->after('user_type');
            $table->integer('total_emissions')->after('food_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diet_foot_print_factors', function (Blueprint $table) {
            //
        });
    }
}
