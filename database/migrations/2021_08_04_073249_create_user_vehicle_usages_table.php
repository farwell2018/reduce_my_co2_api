<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVehicleUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_vehicle_usages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('usage_type')->nullable();
            $table->integer('usage_type_id')->nullable();
            $table->unsignedBigInteger('user_vehicle_eco_profile_id');
            $table->integer('days_per_week')->nullable();
            $table->integer('distance_per_week')->nullable();
            $table->string('vehicle_name');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('user_vehicle_eco_profile_id')->references('id')->on('user_vehicle_eco_profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_vehicle_usages');
    }
}
