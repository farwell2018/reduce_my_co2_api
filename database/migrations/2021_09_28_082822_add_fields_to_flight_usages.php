<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToFlightUsages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('flight_usages')) {
            Schema::table('flight_usages', function (Blueprint $table) {
                if (!Schema::hasColumn('flight_usages', 'destination')) {
                    $table->string('destination')->after('distance')->nullable();
                }
                if (!Schema::hasColumn('flight_usages', 'purpose')) {
                    $table->string('purpose')->after('destination')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('flight_usages')){
            Schema::table('flight_usages', function (Blueprint $table) {
                if (Schema::hasColumn('flight_usages', 'destination')) {
                    $table->dropColumn('destination');
                }
                if (Schema::hasColumn('flight_usages', 'purpose')) {
                    $table->dropColumn('purpose');
                }
            });
        }
    }
}
