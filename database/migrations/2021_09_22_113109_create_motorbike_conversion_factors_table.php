<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMotorbikeConversionFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motorbike_conversion_factors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('motorbike_id');
            $table->integer('measurement_type');
            $table->float('conversion_factor', 8, 8);
            $table->timestamps();

            $table->foreign('motorbike_id')->references('id')->on('motorbikes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motorbike_conversion_factors');
    }
}
