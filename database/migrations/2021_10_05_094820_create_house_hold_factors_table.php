<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHouseHoldFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('house_hold_factors', function (Blueprint $table) {
            $table->id();
            $table->float('electricity', 8, 8)->nullable();
            $table->string('landfill')->nullable();
            $table->string('composting')->nullable();
            $table->float('water', 8, 8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('house_hold_factors');
    }
}
