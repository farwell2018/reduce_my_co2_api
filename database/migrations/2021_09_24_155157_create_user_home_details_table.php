<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserHomeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_home_details', function (Blueprint $table) {
            $table->id();
            $table->integer('lives_in');
            $table->integer('user_id');
            $table->integer('home_size')->nullable();
            $table->integer('no_of_shared_people')->nullable();
            $table->integer('knows_electricity_consumption')->nullable();
            $table->string('electricity_consumption')->nullable();
            $table->integer('heats_house_with')->nullable();
            $table->integer('heat_consumption_amount')->nullable();
            $table->integer('heats_water_with')->nullable();
            $table->integer('knows_water_consumption')->nullable();
            $table->integer('water_consumption_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_home_details');
    }
}
