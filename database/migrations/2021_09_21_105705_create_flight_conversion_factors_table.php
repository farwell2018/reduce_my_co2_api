<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightConversionFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_conversion_factors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('flight_id');
            $table->unsignedBigInteger('flight_class_id');
            $table->integer('measurement_type');
            $table->integer('flight_rf');
            $table->float('conversion_factor', 8, 8);
            $table->timestamps();

            $table->foreign('flight_id')->references('id')->on('flights');
            $table->foreign('flight_class_id')->references('id')->on('flight_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_conversion_factors');
    }
}
