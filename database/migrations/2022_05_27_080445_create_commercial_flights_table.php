<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommercialFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commercial_flights', function (Blueprint $table) {
            $table->id();
            $table->string('manufacturer');
            $table->string('aircraft');
            $table->string('engine');
            $table->integer('fuel_per_hour');
            $table->integer('capacity_seating');
            $table->integer('fuel_per_passenger_hour');
            $table->integer('emissions_per_passenger_hour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commercial_flights');
    }
}
