<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsageTypeIdToUserVehicleCarbonEmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('user_vehicle_carbon_emissions')){
            Schema::table('user_vehicle_carbon_emissions', function (Blueprint $table) {
                if (!Schema::hasColumn('user_vehicle_carbon_emissions', 'usage_type_id')) {
                    $table->integer('usage_type_id')->after('usage_type')->nullable();
                }
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('user_vehicle_carbon_emissions')){
            Schema::table('user_vehicle_carbon_emissions', function (Blueprint $table) {
                if (Schema::hasColumn('user_vehicle_carbon_emissions', 'usage_type_id')) {
                    $table->dropColumn('usage_type_id');
                }
            });
        }
    }
}
