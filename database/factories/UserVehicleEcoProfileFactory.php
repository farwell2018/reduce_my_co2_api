<?php

namespace Database\Factories;

use App\Models\Fuel;
use App\Models\Motorbike;
use App\Models\User;
use App\Models\UserVehicleEcoProfile;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserVehicleEcoProfileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserVehicleEcoProfile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => function () {
                return User::factory()->create()->first()->id;
             },
             'vehicle_name' => 'Scooter',
             'vehicle_type' => function () {
                 return Vehicle::factory()->create()->id;
             },
             'vehicle_fuel_type' => function () {
                 return Fuel::factory()->create()->id;
             },
             'motorbike_type' => function () {
                 return Motorbike::factory()->create()->id;
             }
        ];
    }
}
