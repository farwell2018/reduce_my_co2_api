<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DietFootprintFactorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create seeders for the following:
        // 1. beef
        // 2. lamb
        // 3. prawns
        // 4. fish
        // 5. pork
        // 6. chicken
        // 7. cheese
        // 8. milk
        // 9. eggs
        // 10. beans

        // 1. beef
        $beef = new \App\Models\DietFootPrintFactor();
        $beef->food_type = 'beef';
        $beef->total_emissions = 0.008;
        $beef->save();
        
        // 2. lamb
        $lamb = new \App\Models\DietFootPrintFactor();
        $lamb->food_type = 'lamb';
        $lamb->total_emissions = 0.005;
        $lamb->save();
        
        // 3. prawns
        $prawns = new \App\Models\DietFootPrintFactor();
        $prawns->food_type = 'prawns';
        $prawns->total_emissions = 0.004;
        $prawns->save();

        // 4. fish
        $fish = new \App\Models\DietFootPrintFactor();
        $fish->food_type = 'fish';
        $fish->total_emissions = 0.004;
        $fish->save();

        // 5. pork
        $pork = new \App\Models\DietFootPrintFactor();
        $pork->food_type = 'pork';
        $pork->total_emissions = 0.003;
        $pork->save();

        // 6. chicken
        $chicken = new \App\Models\DietFootPrintFactor();
        $chicken->food_type = 'chicken';
        $chicken->total_emissions = 0.002;
        $chicken->save();

        // 7. cheese
        $cheese = new \App\Models\DietFootPrintFactor();
        $cheese->food_type = 'cheese';
        $cheese->total_emissions = 0.005;
        $cheese->save();

        // 8. milk
        $milk = new \App\Models\DietFootPrintFactor();
        $milk->food_type = 'milk';
        $milk->total_emissions = 0.001;
        $milk->save();

        // 9. eggs
        $eggs = new \App\Models\DietFootPrintFactor();
        $eggs->food_type = 'eggs';
        $eggs->total_emissions = 0.001;
        $eggs->save();

        // 10. beans
        $beans = new \App\Models\DietFootPrintFactor();
        $beans->food_type = 'beans';
        $beans->total_emissions = 0.001;
        $beans->save();

        
    }
}
